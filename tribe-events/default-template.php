<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

// Don't print breadcrumbs
remove_action( 'sa_framework_before_content_area', 'sa_framework_print_page_breadcrumbs' );

// Add events after content
add_action( 'sa_framework_after_content_area', function() {

	?><div id="tribe-events-pg-template">
		<?php tribe_events_before_html(); ?>
		<?php tribe_get_view(); ?>
		<?php tribe_events_after_html(); ?>
	</div> <!-- #tribe-events-pg-template --><?php

});

get_header();

get_footer();
