<?php

// Template Name: SA Stories

// Add stories after content
add_action( 'sa_framework_after_content', function() {

	// The Query
	$the_query = new WP_Query( array(
		'post_type' => 'stories',
		'posts_per_page' => -1,
	) );

	// The Loop
	if ( $the_query->have_posts() ) :

		?><div id="sa-stories"><?php

			while ( $the_query->have_posts() ) :
				$the_query->the_post();
				$post_id = get_the_ID();

				// Build story classes
				$story_classes = array('sa-story');

				// Add story type
				if ( $story_type = get_post_meta( $post_id, 'story_type', true ) ) {
					$story_classes[] = $story_type;
				}

				// Build story details
				$story_details = array(
					'published' => '<span class="story-detail story-published">Published ' . get_the_date( 'F j, Y', $post_id ) . '</span>',
				);

				// Add byline
				if ( $byline = get_post_meta( $post_id, 'byline', true ) ) {
					$story_details[ 'byline' ] = '<span class="story-detail story-byline">' . $byline . '</span>';
				}

				// Get thumbnail
				$caption = null;
				$thumbnail = null;
				$thumbnail_id = get_post_thumbnail_id( $post_id );
				if ( $thumbnail_id && ( $thumbnail = wp_get_attachment_thumb_url( $thumbnail_id ) ) ) {
					$story_classes[] = 'has-side-image';

					// Get caption
					$caption = get_post_field( 'post_excerpt', $thumbnail_id );

				}

				?><div class="<?php echo implode( ' ', $story_classes ); ?>">
					<div class="story-wrapper"><?php

						?><a href="<?php the_permalink(); ?>"><?php

							if ( $thumbnail ) {
								?><img class="story-image" src="<?php echo $thumbnail; ?>" alt="<?php echo $caption; ?>" /><?php
							}

							?><h2 class="story-title"><?php the_title(); ?></h2>

						</a><?php

						// Print details
						if ( $story_details ) {
							?><span class="story-details"><?php echo implode( '', $story_details ); ?></span><?php
						}

						?><div class="story-content"><?php
							the_excerpt();
						?></div><?php

					?></div><!--.story-wrapper-->
				</div><?php

			endwhile;

		?></div><?php

	endif;

	// Restore original Post Data
	wp_reset_postdata();

});

get_header();

get_footer();