<?php

// Set the layout to full width
add_filter( 'sa_framework_page_layout', function( $defined_page_layout ) {
	return 'full-width';
});

get_header();

get_footer();