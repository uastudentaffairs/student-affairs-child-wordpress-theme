<?php

// Setup admin styles and scripts
add_action( 'admin_enqueue_scripts', function ( $hook_suffix ) {
	global $sa_theme_dir;

	// Load depending on the page
	switch( $hook_suffix ) {

		case 'nav-menus.php':

			// Register our menus script
			wp_enqueue_script( 'sa-child-admin-menu', $sa_theme_dir . 'js/sa-child-admin-menu.min.js', array( 'jquery' ), false, true );

			break;

	}

});

// Update custom menu fields
add_action( 'wp_update_nav_menu_item', function( $menu_id, $menu_item_db_id, $args ) {

	// Check if element is properly sent
	if ( isset( $_REQUEST[ 'menu-item-width' ] ) && is_array( $_REQUEST[ 'menu-item-width' ] ) && isset( $_REQUEST[ 'menu-item-width' ][ $menu_item_db_id ] ) ) {

		// Get the value
		$width_value = $_REQUEST[ 'menu-item-width' ][ $menu_item_db_id ];

		// Sanitize the value
		$width_value = preg_replace( '/[^0-9\.]/i', '', $width_value );

		// Store the value
		update_post_meta( $menu_item_db_id, '_menu_item_width', $width_value );

	}

}, 100, 3 );

// Setup AJAX to get custom menu field values
add_action( 'wp_ajax_sa_child_get_menu_width', function() {

	// Get/return menu item width
	if ( isset( $_POST[ 'menu_item_id' ] ) && $_POST[ 'menu_item_id' ] > 0 ) {
		echo get_post_meta( $_POST[ 'menu_item_id' ], '_menu_item_width', true );
	}
	die();

});

// Add options page and field groups
add_action( 'admin_menu', function() {

	// Use the ACF plugin for ease
	if ( function_exists( 'acf_add_options_page' ) ) {

		// Add the options page
		acf_add_options_page( array(
			'page_title' => __( 'Student Affairs: Home Page Settings', 'uastudentaffairs' ),
			'menu_title' => __( 'Home Page Settings', 'uastudentaffairs' ),
			'menu_slug'  => 'student-affairs-child',
			'capability' => 'manage_sa_home_page_settings',
		) );

	}

	if ( function_exists( 'acf_add_local_field_group' ) ):

		// Add field group for archive page
		acf_add_local_field_group( array(
			'key'                   => 'group_5644f31a55dd3',
			'title'                 => 'Archive Page',
			'fields'                => array(
				array(
					'key'               => 'field_5644f32e4094f',
					'label'             => 'Grid Photo',
					'name'              => 'archive_grid_photo',
					'type'              => 'image',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'return_format'     => 'id',
					'preview_size'      => 'sa-archive-grid',
					'library'           => 'all',
					'min_width'         => '',
					'min_height'        => '',
					'min_size'          => '',
					'max_width'         => '',
					'max_height'        => '',
					'max_size'          => '',
					'mime_types'        => '',
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'departments',
					),
				),
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'res_halls',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'left',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => 1,
			'description'           => '',
		) );

		// Add field group for button text
		/*acf_add_local_field_group(array (
			'key' => 'group_569428ae97f43',
			'title' => 'Text for Button',
			'fields' => array (
				array (
					'key' => 'field_56942b2071248',
					'label' => 'Text for Button',
					'name' => 'button_text',
					'type' => 'text',
					'instructions' => 'Sometimes the site will have buttons to lead users to this page. Please include what you would like the button to say, for example "View the 911 Guide".',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
					),
					array (
						'param' => 'page_parent',
						'operator' => '==',
						'value' => '1275',
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
					),
					array (
						'param' => 'page_parent',
						'operator' => '==',
						'value' => '1240',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'side',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));*/

	endif;

}, 10 );