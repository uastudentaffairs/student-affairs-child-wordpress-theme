<?php

//! Print custom fields
add_shortcode( 'print_sa_items', function( $args, $content = null ) {
	global $post;

	// Setup the args
	$defaults = array(
		'ids'       => $post->ID,
		'include_bottom_details' => false,
		'col_medium' => 2,
	);
	$args = wp_parse_args( $args, $defaults );

	// Make sure IDs are an array
	if ( isset( $args[ 'ids' ] ) && ! is_array( $args[ 'ids' ] ) ) {
		$args[ 'ids' ] = explode( ',', $args[ 'ids' ] );
	}

	// Make sure only integers
	$args[ 'ids' ] = array_filter( $args[ 'ids' ], 'intval' );

	// Make sure we have IDs
	if ( ! $args[ 'ids' ] ) {
		return null;
	}

	// Build the content
	$content = '';

	// Print each post
	foreach( $args[ 'ids' ] as $post_id ) {

		// Do we have a department contact ID?
		if ( ( $department_contact_id = get_post_meta( $post_id, 'department_contact_id', true ) )
		     && $department_contact_id > 0 ) {
			$post_id = $department_contact_id;
		}

		// Build each item
		$item = '';

		// Build classes
		$item_classes = array( 'sa-item' );
		//{{#grid_photo}}has-header-img{{/grid_photo}}

		// Should we include bottom details?
		$include_bottom_details = ! ( isset( $args[ 'include_bottom_details' ] ) && ( $args[ 'include_bottom_details' ] == false || $args[ 'include_bottom_details' ] == 0 ) );

		// Add the website
		$website = get_post_meta( $post_id, 'website', true );

		// Set the permalink
		$permalink = ! empty( $website ) ? $website : get_permalink( $post_id );

		// Add title
		$post_title = get_the_title( $post_id );
		if ( $post_title ) {

			// Add the title
			$item .= $post_title;

			// Add the link
			$item = '<a href="' . $permalink . '">' . $item . '</a>';

			// Wrap with title tag
			$item = '<h2 class="item-title">' . $item . '</h2>';

		}

		// Add excerpt
		if ( $excerpt = get_post_field( 'post_excerpt', $post_id ) ) {
			$excerpt = apply_filters( 'the_excerpt', apply_filters( 'get_the_excerpt', $excerpt ) );
			$item .= '<div class="item-content">' . $excerpt . '</div>';
		}

		// Add bottom details
		if ( $include_bottom_details ) {

			// Build bottom details
			$bottom_details = '';

			// Add the website
			if ( $permalink ) {

				// Build website label
				$website_label = ! empty( $post_title ) ? "Learn more about the {$post_title}" : $permalink;

				// Add the website link
				$bottom_details .= '<li class="has-icon has-a website"><a href="' . $permalink . '"><span class="dashicons dashicons-admin-site"></span> <span class="a-label">' . $website_label . '</span></a></li>';

			}

			// Add the office
			if ( $office = get_post_meta( $post_id, 'office', true ) ) {
				$bottom_details .= '<li class="has-icon office"><span class="dashicons dashicons-location"></span> ' . $office . '</li>';
			}

			// Add the phone
			if ( $phone = get_post_meta( $post_id, 'phone', true ) ) {

				// Add the phone
				$bottom_details .= '<li class="has-icon phone"><span class="dashicons dashicons-phone"></span> ' . $phone;

				// Do we have phone TTY?
				if ( $phone_tty = get_post_meta( $post_id, 'phone_tty', true ) ) {
					$bottom_details .= " (Voice), {$phone_tty}  (TTY)";
				}

				// Close the <li>
				$bottom_details .= '</li>';

			}

			// Wrap everything
			if ( $bottom_details ) {

				// Add the details
				$item .= '<ul class="item-details item-bottom-details">' . $bottom_details . '</ul>';

				// Add the class
				if ( $include_bottom_details ) {
					$item_classes[] = 'has-bottom-details';
				}

			}


		}

		// Wrap in <li> and <div>
		$item = '<li><div class="' . implode( ' ', $item_classes ) . '">' . $item . '</div></li>';

		// Add to content
		$content .= $item;

	}

	// Set up <ul,> classes
	$wrapper_classes = array( 'small-block-grid-1', 'sa-items', 'sa-items-grid', 'sa-items-match-height' );

	// What's the big column count?
	$column_count = 1;

	// Add column classes
	if ( isset( $args[ 'col_medium' ] ) && $args[ 'col_medium' ] > 1 ) {
		$wrapper_classes[] = 'medium-block-grid-' . $args[ 'col_medium' ];
		$column_count = $args[ 'col_medium' ];
	}
	if ( isset( $args[ 'col_large' ] ) && $args[ 'col_large' ] > 1 ) {
		$wrapper_classes[] = 'large-block-grid-' . $args[ 'col_large' ];
		$column_count = $args[ 'col_large' ];
	}

	// Wrap the content
	$content = '<ul id="sa-inclusion-grid" class="' . implode( ' ', $wrapper_classes ) . '"' . ( $column_count > 1 ? ' data-columns="' . $column_count . '"' : null ) . '>' . $content . '</ul>';

	// Return the content
	return $content;

});