<?php

// Filter the hero image
add_filter( 'sa_framework_hero_image_source', function( $hero_image_src ) {
	return is_singular( 'stories' ) ? 'https://sa.ua.edu/wp-content/uploads/Hero_DennyChimes.png' : $hero_image_src;
});

// Filter the page section post ID
add_filter( 'sa_framework_page_section_post_id', function( $page_section_post_id ) {
	return ( is_singular( 'stories' ) && ( $stories_page = get_page_by_path( 'our-stories' ) ) ) ? $stories_page->ID : $page_section_post_id;
});

// Make sure events always have a post type archive title
add_filter( 'post_type_archive_title', function( $archive_title, $post_type ) {
	return ( 'tribe_events' == $post_type ) ? 'Our Stories' : $archive_title;
}, 100, 2 );

// Always show the page title for stories
add_filter( 'sa_framework_display_page_title', function( $display_page_title ) {
	return is_singular( 'stories' ) ? true: $display_page_title;
}, 100000000 );

// Add statement after stories
add_filter( 'the_content', function( $post_content ) {
	global $post;

	if ( 'stories' != $post->post_type ) {
		return $post_content;
	}

	if ( ! is_singular( 'stories' ) ) {
		return $post_content;
	}

	$post_content .= '<p class="stories-footer">This story is a part of Student Affairs Our Stories, which spotlights students, staff, services and programs that are excelling within the Division of Student Affairs. Do you have a suggestion for a Student Affairs story? Email <a href="mailto:studentaffairs@ua.edu">studentaffairs@ua.edu</a> with your idea.</p>';

	return $post_content;
});