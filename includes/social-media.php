<?php

global $sa_twitter_accounts, $sa_instagram_accounts;

$sa_twitter_accounts = array( 'uastudents', 'bamavpsa', 'uahousing', 'bamaparents', 'uacareercenter', 'alabamaurec', 'thefergua', 'UAinvolvement', 'uawhatsup', 'UAVETS', 'uaofsl' );
$sa_instagram_accounts = array( '245315050', '1934718827', '2160761520', '1491649112', '1102080642', '2216753778' );

// Instagram IDs
// 245315050 = uastudentaffairs
// 1934718827 = univofalabama
// 2160761520 = UREC
// 1491649112 = Career
// 1102080642 = Vets
// 2216753778 = Ferg

// Gather social media
// @TODO setup to only run every so often
add_action( 'wp', function( $wp ) {
	global $sa_twitter_accounts, $sa_instagram_accounts;

	// Don't run on API calls
	$json_route = get_query_var( 'json_route' );
	if ( $json_route ) {
		return;
	}

	if ( ! is_super_admin() ) {
		return;
	}

	// Gather all the tweets
	foreach( $sa_twitter_accounts as $account ) {
		gather_sa_tweets( $account, true );
	}

	// Gather all the instagrams
	foreach( $sa_instagram_accounts as $account ) {
		gather_sa_instagram( $account, true );
	}

});

function gather_last_sa_tweet( $screen_name, $force_update = false ) {

	// Setup the transient
	$tweets_transient = "sa_last_tweet_{$screen_name}";

	// Pull and return from transient
	if ( ! $force_update ) {

		// Get last tweet from transient
		$sa_last_tweet_transient = get_site_transient( $tweets_transient );

		// If proper value, return stored value
		if ( false !== $sa_last_tweet_transient ) {
			return $sa_last_tweet_transient;
		}

	}

	// Setup args
	$args = array( 'count' => 1 );

	if ( 'uastudents' != $screen_name ) {
		$args[ 'screen_name' ] = $screen_name;
	}

	if ( function_exists( 'a_social_gathering_twitter' )
	     && ( $tweets = a_social_gathering_twitter()->get_user_timeline( 'uastudents', $args ) ) ) {

		// Set item type
		foreach( $tweets as &$tweet ) {
			$tweet->item_type = 'twitter';
		}

		// Make sure we have a tweet
		if ( isset( $tweets[0] ) && ! empty( $tweets[0] ) ) {

			// Store most recent in transient
			set_site_transient( $tweets_transient, $tweets[0], 900 ); // Every 15 minutes

			return $tweets[0];

		}

	}

	return false;

}

function gather_sa_tweets( $screen_name, $force_update = false ) {

	// Setup the transient
	$tweets_transient = "sa_tweets_{$screen_name}";

	// Pull and return from transient
	if ( ! $force_update ) {

		// Get latest stored value
		$sa_tweets_transient = get_site_transient( $tweets_transient );

		// Return stored value
		if ( false !== $sa_tweets_transient ) {
			return $sa_tweets_transient;
		}

	}

	// Setup args
	$args = array( 'count' => 20 );

	if ( 'uastudents' != $screen_name ) {
		$args[ 'screen_name' ] = $screen_name;
	}

	if ( function_exists( 'a_social_gathering_twitter' )
	     && ( $tweets = a_social_gathering_twitter()->get_user_timeline( 'uastudents', $args ) ) ) {

		// Don't process if errors
		if ( ! isset( $tweets->errors ) ) {

			// Set item type
			foreach ( $tweets as &$tweet ) {
				$tweet->item_type = 'twitter';
			}

			if ( isset( $tweets ) && ! empty( $tweets ) ) {

				// Store most recent in transient
				if ( isset( $tweets[0] ) && ! empty( $tweets[0] ) ) {
					set_site_transient( "sa_last_tweet_{$screen_name}", $tweets[0], 900 ); // Every 15 minutes
				}

				// Update transient - never expires
				set_site_transient( $tweets_transient, $tweets, 900 ); // Every 15 minutes

				return $tweets;

			}

		}

	}

	return false;
}

function gather_sa_instagram( $screen_name_or_id, $force_update = false ) {

	// Setup the transient
	$instagram_transient = "sa_instagram_{$screen_name_or_id}";

	// Get last instagram
	$sa_instagram_transient = get_site_transient( $instagram_transient );

	// Pull and return from transient
	if ( ! $force_update && false !== $sa_instagram_transient ) {
		return $sa_instagram_transient;
	}

	if ( function_exists( 'a_social_gathering_instagram' ) ) {

		if ( 'uastudentaffairs' == $screen_name_or_id ) {
			$instagram = a_social_gathering_instagram()->get_self_recent( 'uastudentaffairs', array( 'count' => 20 ) );
		} else if ( is_numeric( $screen_name_or_id ) ) {
			$instagram = a_social_gathering_instagram()->get_user_recent( $screen_name_or_id, 'uastudentaffairs', array( 'count' => 20 ) );
		}

		if ( $instagram ) {

			// Set item type
			foreach ( $instagram as &$item ) {
				$item->item_type = 'instagram';
			}

			if ( isset( $instagram ) && ! empty( $instagram ) ) {

				// Store most recent in transient
				if ( isset( $instagram[0] ) && ! empty( $instagram[0] ) ) {
					set_site_transient( "sa_last_instagram_{$screen_name_or_id}", $instagram[0], 900 ); // Every 15 minutes
				}

				// Update transient - never expires
				set_site_transient( $instagram_transient, $instagram, 900 ); // Every 15 minutes

				return $instagram;

			}

		}

	}

	return false;
}

// Get tweets and instagram
function gather_sa_social_media_grid_items( $args = array() ) {
	global $sa_twitter_accounts, $sa_instagram_accounts;

	// Setup arguments
	$defaults = array(
		'instagram' => array( 'uastudentaffairs', '1934718827' ), // univofalabama
		'twitter' => array( 'uastudents' ),
		'get_default_twitter' => false, // If false, won't also pull SA account
		'get_default_instagram' => false, // If false, won't also pull SA and UA accounts
	);
	$args = wp_parse_args( $args, $defaults );


	// Pull default twitter
	if ( $args[ 'get_default_twitter' ] ) {
		$args[ 'twitter' ] = $sa_twitter_accounts;
	} else {

		// Merge with default settings
		$args[ 'twitter' ] = array_unique( array_merge( $args[ 'twitter' ], $defaults[ 'twitter' ] ) );

	}

	// Pull default instagram
	if ( $args[ 'get_default_instagram' ] ) {
		$args[ 'instagram' ] = $sa_instagram_accounts;
	} else {

		// Merge with default settings
		$args[ 'instagram' ] = array_unique( array_merge( $args[ 'instagram' ], $defaults[ 'instagram' ] ) );

	}

	// Get social media
	$social_media = array();

	// Get student affairs tweets
	foreach( $args[ 'twitter' ] as $account ) {

		if ( $tweets = gather_sa_tweets( $account ) ) {

			// Add to list
			$social_media = array_merge( $social_media, $tweets );

		}

	}

	// Get instagrams
	foreach( $args[ 'instagram' ] as $account ) {

		if ( $instagram = gather_sa_instagram( $account ) ) {

			// Add to list
			$social_media = array_merge( $social_media, $instagram );

		}

	}

	// Sort the social media
	usort( $social_media, 'ua_sa_main_site_sort_social_media_desc' );

	return $social_media;
}

// Get tweets and instagram
function get_sa_social_media_grid( $args = array() ) {

	// We store the social media
	$social_media_transient_name = 'sa_social_media_grid_html';

	// Pull and return from transient
	$social_media_transient = get_transient( $social_media_transient_name );
	if ( false !== $social_media_transient ) {
		return $social_media_transient;
	}

	// Get items
	$social_media = gather_sa_social_media_grid_items( $args );

	// How many items?
	$grid_item_max = 10;

	// Limit the amount of tweets
	$twitter_max = 10 / 2;

	// Build array of items
	$instagram_items = array();
	$twitter_items = array();

	// Process the items
	$processed_items = 0;
	foreach( $social_media as $media_item ) {

		// Don't process more than 10
		if ( $processed_items >= $grid_item_max ) {
			break;
		}

		// Build grid item
		$grid_item = null;

		// If we're instagram and have a media
		if ( 'instagram' == $media_item->item_type && ! in_array( "ig-{$media_item->id}", $instagram_items ) ) {

			if ( isset( $media_item->images )
				&& isset( $media_item->images->standard_resolution )
			    && isset( $media_item->images->standard_resolution->url ) ) {

				// Get user
				$instagram_username = isset( $media_item->user->username ) ? $media_item->user->username : false;

				// Add grid item
				$instagram_items[ "ig-{$media_item->id}" ] = '<div class="grid-item instagram" style="background-image:url(' . $media_item->images->standard_resolution->url . ');"><a href="' . $media_item->link . '"><div class="grid-item-user">@' . $instagram_username . '</div></a></div>';
				$processed_items++;

			}

		}

		else if ( 'twitter' == $media_item->item_type && ! in_array( "tw-{$media_item->id}", $twitter_items ) && count( $twitter_items ) < $twitter_max ) {

			// Is this a retweet?
			$retweet = isset( $media_item->retweeted_status ) ? true : false;

			// If RT, then get the RT info
			if ( $the_tweet = $retweet ? $media_item->retweeted_status : $media_item ) {

				// Make sure we have text
				if ( $the_tweet_text = $the_tweet->text ) {

					// Get the tweet's user name
					$the_tweet_user_name = isset( $the_tweet->user->screen_name ) ? $the_tweet->user->screen_name : NULL;

					// What's the URL to their twitter profile?
					$the_tweet_user_url = "https://twitter.com/{$the_tweet_user_name}";

					// What's the URL to the tweet?
					$the_tweet_url = $the_tweet_user_url . '/status/' . $the_tweet->id;

					// Convert links in tweets to actual links
					$the_tweet_text = preg_replace( '/((http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/[a-z0-9\-]*)?)/i', '<a href="$1" target="_blank">$1</a>', $the_tweet_text );

					// Convert twitter handles to links to profile
					$the_tweet_text = preg_replace( '/(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([a-z0-9_]+)/i', '<a href="https://twitter.com/$1" target="_blank">@$1</a>', $the_tweet_text );

					// Add tweet text
					$grid_item .= '<div class="tweet">' . iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', $the_tweet_text ) . '<div class="grid-item-user"><a href="' . $the_tweet_url . '"><span class="dashicons dashicons-twitter"></span> <span class="username">@' . $the_tweet_user_name . '</span></a></div></div>';

					// Add grid item
					$twitter_items["tw-{$media_item->id}"] = '<div class="grid-item twitter">' . $grid_item . '</div>';
					$processed_items++;

				}

			}

			/*if ( isset( $media_item->entities )
			     && isset( $media_item->entities->media ) ) {

				// Get first media item
				$first_twitter_media_item = array_shift( $media_item->entities->media );

				if ( isset( $first_twitter_media_item->media_url_https ) ) {
					$media_url = $first_twitter_media_item->media_url_https;
				}

			}*/

		}

	}

	// Reset keys
	$instagram_items = array_values( $instagram_items );
	$twitter_items = array_values( $twitter_items );

	// Create grid
	$social_media_grid_items = array();
	for( $i=0; $i < $grid_item_max; $i++ ) {

		if ( count( $social_media_grid_items ) >= $grid_item_max ) {
			break;
		}

		// Photo first
		if ( isset( $instagram_items[$i]) ) {
			$social_media_grid_items[] = $instagram_items[$i];
		}
		// The tweet
		if ( isset( $twitter_items[$i]) ) {
			$social_media_grid_items[] = $twitter_items[$i];
		}

	}

	// Build social media grid HTML
	$social_media_grid = ! empty( $social_media_grid_items ) ? ( '<div id="sa-social-media-grid">' . implode( '', $social_media_grid_items ) . '</div>' ) : null;

	// Set transient
	if ( ! empty( $social_media_grid ) ) {
		set_transient( $social_media_transient_name, $social_media_grid, 900 ); // Every 15 minutes
	}

	return $social_media_grid;

	// INSTAGRAM
	/*[attribution] =>
    [tags] => Array
        (
            [0] => auntraerae
        )

    [type] => image
    [location] =>
    [comments] => stdClass Object
        (
            [count] => 0
            [data] => Array
                (
                )

        )

    [filter] => Normal
    [created_time] => 1439646892
    [link] => https://instagram.com/p/6aAxUlDWPY/
    [likes] => stdClass Object
        (
            [count] => 13
            [data] => Array
                (
                    [0] => stdClass Object
                        (
                            [username] => vosida28nis_nonolit
                            [profile_picture] => https://scontent.cdninstagram.com/hphotos-xfp1/t51.2885-19/11055661_1608243736076189_2129397050_a.jpg
                            [id] => 1794503449
                            [full_name] =>
                        )

                    [1] => stdClass Object
                        (
                            [username] => joshuachollis
                            [profile_picture] => https://scontent.cdninstagram.com/hphotos-xtf1/t51.2885-19/11850130_420727481452256_2007491228_a.jpg
                            [id] => 724936711
                            [full_name] => Joshua Hollis
                        )

                )

        )

    [images] => stdClass Object
        (
            [low_resolution] => stdClass Object
                (
                    [url] => https://scontent.cdninstagram.com/hphotos-xfp1/t51.2885-15/s320x320/e35/11881690_749880165122728_1892288250_n.jpg
                    [width] => 320
                    [height] => 320
                )

            [thumbnail] => stdClass Object
                (
                    [url] => https://scontent.cdninstagram.com/hphotos-xfp1/t51.2885-15/s150x150/e35/11881690_749880165122728_1892288250_n.jpg
                    [width] => 150
                    [height] => 150
                )

            [standard_resolution] => stdClass Object
                (
                    [url] => https://scontent.cdninstagram.com/hphotos-xfp1/t51.2885-15/s640x640/sh0.08/e35/11881690_749880165122728_1892288250_n.jpg
                    [width] => 640
                    [height] => 640
                )

        )

    [users_in_photo] => Array
        (
            [0] => stdClass Object
                (
                    [position] => stdClass Object
                        (
                            [y] => 0.288
                            [x] => 0.801333333
                        )

                    [user] => stdClass Object
                        (
                            [username] => silasandmiri
                            [profile_picture] => https://igcdn-photos-g-a.akamaihd.net/hphotos-ak-xtp1/t51.2885-19/s150x150/12080629_907292582691566_1651528707_a.jpg
                            [id] => 1596509
                            [full_name] => britt cherry
                        )

                )

            [1] => stdClass Object
                (
                    [position] => stdClass Object
                        (
                            [y] => 0.553333333
                            [x] => 0.282666667
                        )

                    [user] => stdClass Object
                        (
                            [username] => joshuachollis
                            [profile_picture] => https://scontent.cdninstagram.com/hphotos-xtf1/t51.2885-19/11850130_420727481452256_2007491228_a.jpg
                            [id] => 724936711
                            [full_name] => Joshua Hollis
                        )

                )

        )

    [caption] => stdClass Object
        (
            [created_time] => 1439646892
            [text] => Playtime with my niece Miri, @joshuachollis, Cinderelly, and Jerry the Giraffe. #AuntRaeRae
            [from] => stdClass Object
                (
                    [username] => bamadesigner
                    [profile_picture] => https://scontent.cdninstagram.com/hphotos-xtf1/t51.2885-19/10948756_1452069978436042_965351259_a.jpg
                    [id] => 2530855
                    [full_name] => Rachel Cherry Carden
                )

            [id] => 1052156854040093188
        )

    [user_has_liked] =>
    [id] => 1052156852295263192_2530855
    [user] => stdClass Object
        (
            [username] => bamadesigner
            [profile_picture] => https://scontent.cdninstagram.com/hphotos-xtf1/t51.2885-19/10948756_1452069978436042_965351259_a.jpg
            [id] => 2530855
            [full_name] => Rachel Cherry Carden
        )

    [item_type] => instagram*/

	// TWITTER
	/*[created_at] => Sun Nov 08 20:25:22 +0000 2015
    [id] => 663452271099932672
    [id_str] => 663452271099932672
    [text] => It's almost time for Serve Better Together 2015- a day of service hosted by @Crossroads_UA! Sign up to participate: https://t.co/0ladtyDUdI
    [source] => Hootsuite
    [truncated] =>
    [in_reply_to_status_id] =>
    [in_reply_to_status_id_str] =>
    [in_reply_to_user_id] =>
    [in_reply_to_user_id_str] =>
    [in_reply_to_screen_name] =>
    [user] => stdClass Object
        (
            [id] => 355722293
            [id_str] => 355722293
            [name] => UA Student Affairs
            [screen_name] => UAStudents
            [location] => The University of Alabama
            [description] => Your source for information from UA's Division of Student Affairs. Stay tuned for all the latest updates! Roll Tide!
http://t.co/Z8bKcwLfdh
            [url] => http://t.co/KMf07dFMb0
            [entities] => stdClass Object
                (
                    [url] => stdClass Object
                        (
                            [urls] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [url] => http://t.co/KMf07dFMb0
                                            [expanded_url] => http://www.facebook.com/UAStudentAffairs
                                            [display_url] => facebook.com/UAStudentAffaiâ€¦
                                            [indices] => Array
                                                (
                                                    [0] => 0
                                                    [1] => 22
                                                )

                                        )

                                )

                        )

                    [description] => stdClass Object
                        (
                            [urls] => Array
                                (
                                    [0] => stdClass Object
                                        (
                                            [url] => http://t.co/Z8bKcwLfdh
                                            [expanded_url] => https://sa.ua.edu
                                            [display_url] => sa.ua.edu
                                            [indices] => Array
                                                (
                                                    [0] => 117
                                                    [1] => 139
                                                )

                                        )

                                )

                        )

                )

            [protected] =>
            [followers_count] => 6362
            [friends_count] => 4139
            [listed_count] => 47
            [created_at] => Mon Aug 15 19:55:12 +0000 2011
            [favourites_count] => 948
            [utc_offset] => -21600
            [time_zone] => Central Time (US & Canada)
            [geo_enabled] =>
            [verified] =>
            [statuses_count] => 4640
            [lang] => en
            [contributors_enabled] =>
            [is_translator] =>
            [is_translation_enabled] =>
            [profile_background_color] => 131516
            [profile_background_image_url] => http://abs.twimg.com/images/themes/theme14/bg.gif
            [profile_background_image_url_https] => https://abs.twimg.com/images/themes/theme14/bg.gif
            [profile_background_tile] => 1
            [profile_image_url] => http://pbs.twimg.com/profile_images/514816773355753472/fg4swHLv_normal.jpeg
            [profile_image_url_https] => https://pbs.twimg.com/profile_images/514816773355753472/fg4swHLv_normal.jpeg
            [profile_banner_url] => https://pbs.twimg.com/profile_banners/355722293/1354118333
            [profile_link_color] => 009999
            [profile_sidebar_border_color] => EEEEEE
            [profile_sidebar_fill_color] => EFEFEF
            [profile_text_color] => 333333
            [profile_use_background_image] => 1
            [has_extended_profile] =>
            [default_profile] =>
            [default_profile_image] =>
            [following] =>
            [follow_request_sent] =>
            [notifications] =>
        )

    [geo] =>
    [coordinates] =>
    [place] =>
    [contributors] =>
    [is_quote_status] =>
    [retweet_count] => 2
    [favorite_count] => 2
    [entities] => stdClass Object
        (
            [hashtags] => Array
                (
                )

            [symbols] => Array
                (
                )

            [user_mentions] => Array
                (
                    [0] => stdClass Object
                        (
                            [screen_name] => Crossroads_UA
                            [name] => CrossroadsUA
                            [id] => 1228141045
                            [id_str] => 1228141045
                            [indices] => Array
                                (
                                    [0] => 76
                                    [1] => 90
                                )

                        )

                )

            [urls] => Array
                (
                    [0] => stdClass Object
                        (
                            [url] => https://t.co/0ladtyDUdI
                            [expanded_url] => http://ow.ly/UkCk0
                            [display_url] => ow.ly/UkCk0
                            [indices] => Array
                                (
                                    [0] => 116
                                    [1] => 139
                                )

                        )

                )

        )

    [favorited] =>
    [retweeted] =>
    [possibly_sensitive] =>
    [lang] => en
    [created_time] => 1447014322
    [item_type] => twitter*/

}

// Add most recent tweet to sidebar
add_action( 'sa_framework_before_left_sidebar', function() {
	global $blog_id;

	// What screen name?
	$screen_name = null;

	// Is VP page?
	$is_vp_page = is_page( 'vice-president' );

	switch( $blog_id ) {

		// Main site
		case 1:
			$screen_name = $is_vp_page ? 'bamavpsa' : 'uastudents';
			break;

		// Housing
		case 2:
			$screen_name = 'uahousing';
			break;

		// Parents
		case 3:
			$screen_name = 'bamaparents';
			break;

		// Career Center
		case 4:
			$screen_name = 'uacareercenter';
			break;

		// UREC
		case 5:
			$screen_name = 'alabamaurec';
			break;

		// Veteran and Military Affairs
		case 6:
			$screen_name = 'UAVETS';
			break;

		// OFSL
		case 9:
			$screen_name = 'uaofsl';
			break;

		// Default to main account
		default:
			$screen_name = 'uastudents';
			break;

	}

	if ( ( $last_tweet = gather_last_sa_tweet( $screen_name ) )
		&& ( $printed_tweets = print_sa_tweets( array( $last_tweet ) ) ) ) {

		?><div class="sidebar-item"><?php

			if ( $is_vp_page ) {
				?><h3 class="sidebar-header tweets-header"><a href="https://twitter.com/BamaVPSA/">Follow Dr. Grady on Twitter</a></h3><?php
			}

			echo $printed_tweets;

		?></div><?php

	}

}, 10 );

function print_sa_tweets( $tweets, $args = array() ) {

	// Build the tweet content
	$printed_tweets = NULL;

	// Make sure there are tweets
	if ( ! ( isset( $tweets ) && ! empty( $tweets ) ) ) {
		return $printed_tweets;
	}

	// Setup arguments
	$arg_defaults = array(
		'container_class' => '',
		'show_retweeted' => true,
	);
	$args = wp_parse_args( $args, $arg_defaults );

	// Get count from the tweet args. The default is 5.
	$count = isset( $tweet_args[ 'count' ] ) && $tweet_args[ 'count' ] > 0 ? $tweet_args[ 'count' ] : 5;

	// Set up <ul> class
	$container_class = 'sa-tweets';

	if ( ! empty ( $args[ 'container_class' ] ) ) {
		$container_class .= ' ' . $args[ 'container_class' ];
	}

	// Build the printed tweets
	$printed_tweets .= '<ul class="' . $container_class . '">';

		// Keeps track of tweets so we can limit count
		$tweet_index = 1;

		// Are we on SSL?
		$is_ssl = is_ssl();

		foreach( $tweets as $the_tweet_data ) {

			// Don't go over the set count
			if ( $tweet_index > $count ) {
				break;
			}

			// Is this a retweet?
			$retweet = isset( $the_tweet_data->retweeted_status ) ? true : false;

			// If RT, then get the RT info
			if ( $the_tweet = $retweet ? $the_tweet_data->retweeted_status : $the_tweet_data ) {

				// Make sure we have text
				if ( $the_tweet_text = $the_tweet->text ) {

					// Does this tweet have an image?
					$tweet_image = false;
					if ( isset( $the_tweet->extended_entities->media ) ) {
						foreach( $the_tweet->extended_entities->media as $media ) {

							// Make sure its a photo
							if ( 'photo' == $media->type ) {
								$tweet_image = $is_ssl ? $media->media_url_https : $media->media_url;

								// If we have indices, remove text link to photo
								if ( isset( $media->indices ) && isset( $media->indices[0] ) && isset( $media->indices[1] ) ) {
									$the_tweet_text = trim( substr_replace( $the_tweet_text, '', $media->indices[0], $media->indices[1] ) );
								}

								break;
							}

						}
					}

					/*[indices] => Array
		                (
		                    [0] => 90
		                    [1] => 113
		                )

		            [media_url] => http://pbs.twimg.com/media/CTp8GcIUAAMFsNh.jpg
		            [media_url_https] => https://pbs.twimg.com/media/CTp8GcIUAAMFsNh.jpg
		            [url] => https://t.co/e5j32uv8wB
		            [display_url] => pic.twitter.com/e5j32uv8wB
		            [expanded_url] => http://twitter.com/Jordan__Forrest/status/664980393284771840/photo/1
		            [type] => photo
		            [sizes] => stdClass Object
		                (
		                    [small] => stdClass Object
		                        (
		                            [w] => 340
		                            [h] => 255
		                            [resize] => fit
		                        )

		                    [medium] => stdClass Object
		                        (
		                            [w] => 600
		                            [h] => 450
		                            [resize] => fit
		                        )

		                    [thumb] => stdClass Object
		                        (
		                            [w] => 150
		                            [h] => 150
		                            [resize] => crop
		                        )

		                    [large] => stdClass Object
		                        (
		                            [w] => 1024
		                            [h] => 768
		                            [resize] => fit
		                        )

		                )*/

					// Get the tweet's user full name
					//$the_tweet_user_full_name = isset( $the_tweet->user->name ) ? $the_tweet->user->name : NULL;

					// Get the tweet's user name
					$the_tweet_user_name = isset( $the_tweet->user->screen_name ) ? $the_tweet->user->screen_name : NULL;

					//  Get the tweet's user profile image
					//$the_tweet_profile_img = $is_ssl ? ( isset( $the_tweet->user->profile_image_url_https ) ? $the_tweet->user->profile_image_url_https : NULL ) : ( isset( $the_tweet->user->profile_image_url ) ? $the_tweet->user->profile_image_url : NULL );

					// What's the URL to their twitter profile?
					$the_tweet_user_url = "https://twitter.com/{$the_tweet_user_name}";

					// What's the URL to the tweet?
					$the_tweet_url = $the_tweet_user_url . '/status/' . $the_tweet->id;

					// Convert links in tweets to actual links
					$the_tweet_text = preg_replace( '/((http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/[a-z0-9\-]*)?)/i', '<a href="$1" target="_blank">$1</a>', $the_tweet_text );

					// Convert twitter handles to links to profile
					$the_tweet_text = preg_replace( '/(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([a-z0-9_]+)/i', '<a href="https://twitter.com/$1" target="_blank">@$1</a>', $the_tweet_text );

					// Assign classes for the tweet
					$tweet_classes = array( 'tweet' );

					// Start printing the tweet
					$printed_tweets .= '<li class="' . implode( ' ', $tweet_classes ) . '">';

						/*// If we have a user name...
						if ( $the_tweet_user_full_name ) {

							$printed_tweets .= '<span class="header">';

                                $printed_tweets .= '<a href="' . $the_tweet_user_url . '" target="_blank">';

                                    // Add the profile image
                                    if ( $the_tweet_profile_img ) {
                                        $printed_tweets .= '<img class="profile-image" src="' . $the_tweet_profile_img . '" alt="" />';
                                    }

                                    // Add the handle
                                    $printed_tweets .= '<span class="handle">
                                        <span class="full-name">' . $the_tweet_user_full_name . '</span>';

                                        if ( $the_tweet_user_name ) {
                                            $printed_tweets .= '<span class="screen_name">@' . $the_tweet_user_name . '</span>';
                                        }
                                    $printed_tweets .= '</span>';

                                $printed_tweets .= '</a>';

								// Print the created date
								$printed_tweets .= '<a class="created" href="' . $the_tweet_url . '" target="_blank">';

									$time_diff = $right_now - $the_tweet_created_at;

									// Less than an hour
									if ( $time_diff < 3600 )
										$printed_tweets .= floor( ( $right_now - $the_tweet_created_at ) / 60 ) . 'm';

									// Less than a day
									else if ( $time_diff < 86400 )
										$printed_tweets .= floor( ( $right_now - $the_tweet_created_at ) / 3600 ) . 'h';

									else
										$printed_tweets .= date( 'j M', $the_tweet_created_at );

								$printed_tweets .= '</a>';

							$printed_tweets .= '</span>';

						}*/

						if ( $tweet_image ) {
							$printed_tweets .= '<div class="tweet-photo" style="background-image:url(\'' . $tweet_image . '\');"><a href="' . $the_tweet_url . '" target="_blank"><span class="screen-reader-text">View this tweet</span></a></div>';
						}

						// Print the tweet text
						$printed_tweets .= '<span class="text">' . iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', $the_tweet_text ) . '</span>';

						// Print the footer
						// If we have a user name...
						$printed_tweets .= '<span class="footer">';

							$printed_tweets .= '<a href="' . $the_tweet_url . '" target="_blank">';

								// Add the user name
								if ( $the_tweet_user_name ) {
									$printed_tweets .= '<span class="handle"><span class="dashicons dashicons-twitter"></span> <span class="screen_name">@' . $the_tweet_user_name . '</span></span>';
								}

							$printed_tweets .= '</a>';

						$printed_tweets .= '</span>';

						// If this is a retweet
						if ( $args[ 'show_retweeted' ] && $retweet && isset( $the_tweet_data->user->name ) && ( $retweeted_by = $the_tweet_data->user->name ) ) {

							// What is the retweeted user URL?
							$retweeted_by_twitter_user_url = isset( $the_tweet_data->user->screen_name ) ? ( "https://twitter.com/{$the_tweet_data->user->screen_name}" ) : NULL;

							// If we don't have a user URL, then change the element
							$el = $retweeted_by_twitter_user_url ? 'a' : 'span';

							// Print the retweeted data
							$printed_tweets .= '<' . $el . ' class="retweet"';

								// Include the user URL
								if ( $retweeted_by_twitter_user_url ) {
									$printed_tweets .= ' href="' . $retweeted_by_twitter_user_url . '" target="_blank"';
								}

							$printed_tweets .= '>';

								$printed_tweets .= '<span class="retweeted-by">Retweeted by</span> <span class="full-name">' . $retweeted_by . '</span>';

							$printed_tweets .= '</' . $el . '>';

						}

					$printed_tweets .= '</li>';

					// Keep track of which tweet we're on
					$tweet_index++;

				}

			}

		}

	$printed_tweets .= '</ul>';

	return $printed_tweets;

}

function ua_sa_main_site_sort_social_media_desc( $a, $b ) {
	if ( isset( $a->created_at ) ) {
		$a->created_time = strtotime( $a->created_at );
	}
	if ( isset( $b->created_at ) ) {
		$b->created_time = strtotime( $b->created_at );
	}
	return ( $a->created_time == $b->created_time ) ? 0 : ( ( $a->created_time > $b->created_time ) ? -1 : 1 );
}