<?php

// Set the page layout
add_filter( 'sa_framework_page_layout', function( $page_layout ) {

	// Set all events pages to full width column
	// Set 404s to full width
	if ( is_post_type_archive( 'tribe_events' ) || is_404() ) {
		return 'full-width';
	}

	return $page_layout;
});

// Print the header
add_action( 'sa_framework_print_header', 'sa_child_print_header' );
function sa_child_print_header() {
	global $sa_framework_dir, $sa_framework_site_slug;

	// Is this the main site?
	$is_main_site = is_sa_main_site();

	// Setup header wrapper classes
	$header_wrapper_classes = array( "sa-{$sa_framework_site_slug}" );

	?><div id="sa-header-wrapper" class="<?php echo implode( ' ', $header_wrapper_classes ); ?>">
		<div class="row">
			<div id="sa-header">

				<div class="small-12 medium-3 columns sa-logo-column">
					<div class="sa-logo-wrapper">
						<a href="https://sa.ua.edu/">
							<img class="sa-logo" src="<?php echo $sa_framework_dir; ?>images/logos/CapstoneA-SA.svg" alt="" />
							<span class="screen-reader-text">The University of Alabama Division of Student Affairs</span>
						</a>
					</div>
				</div>

				<div class="small-12 medium-9 columns sa-title-column"><?php

					// For main site
					if ( $is_main_site ) {

					} else {

						do_action( 'sa_framework_before_site_title' );

						?><div class="sa-site-title-wrapper">
							<a href="<?php echo get_bloginfo( 'url' ); ?>">
								<h1 class="sa-site-title"><?php echo apply_filters( 'sa_framework_header_site_title', get_bloginfo( 'title' ) ); ?></h1>
							</a>
						</div><?php

						do_action( 'sa_framework_after_site_title' );

					}

				?></div>

			</div> <!-- #sa-header -->
		</div>
	</div> <!-- #sa-header-wrapper --><?php

}

// Add the home icon to the main menu
add_action( 'sa_framework_after_main_menu', function ( $main_menu_args ) {

	if ( ! is_front_page() ) {
		?><a id="sa-main-menu-home" href="<?php echo get_bloginfo( 'url' ); ?>">Home</a><?php
	}

});

// Add the dropddown element after the menu
add_action( 'sa_framework_after_main_menu_wrapper', function() {

	?><div id="sa-main-menu-dropdown-wrapper">
		<div class="row">
			<div class="small-12 columns">
				<ul id="sa-main-menu-dropdown"></ul>
			</div>
		</div>
	</div><?php

}, 0 );

// Get custom menu attributes
add_filter( 'wp_setup_nav_menu_item', function ( $menu_item ) {
	$menu_item->width = preg_replace( '/[^0-9\.]/i', '', get_post_meta( $menu_item->ID, '_menu_item_width', true ) );
	return $menu_item;
});

// Tweak the main menu <li> items to get the widths
add_filter( 'nav_menu_li_attributes', function( $li_atts, $item, $args, $depth ) {

	// Not for the off canvas main menu
	if ( ! empty($args->menu_id) && 'sa-off-canvas-main-menu' == $args->menu_id ) {
		return $li_atts;
	}

	// Add style attribute for width
	if ( isset( $item->width ) && $item->width > 0 ) {
		$style = 'width:' . $item->width . '%;';
		if ( ! isset( $li_atts[ 'style' ] ) ) {
			$li_atts[ 'style' ] = $style;
		} else {
			$li_atts[ 'style' ] .= $style;
		}
	}

	return $li_atts;
}, 100, 4 );

// Add the feature photo
add_action( 'sa_framework_before_main_area', function() {
	global $post, $sa_theme_dir;

	// Is this the main site?
	$is_main_site = is_sa_main_site();

	// Set the hero image
	$hero_image_src = null;
	$hero_image_id = null;
	$load_hero_image_id = null;

	// For home page on main site
	if ( is_front_page() ) {

		if ( $is_main_site ) {
			$hero_image_src = $sa_theme_dir . 'images/SA-homepage.jpg';
		} else {

			// Get hero image ID
			// Have to get image source since not a hero image CPT
			if ( ( $hero_image = wp_get_attachment_image_src( get_option( 'options_front_page_hero_image' ), 'sa-hero-front-page' ) )
				&& isset( $hero_image[0] ) ) {
				$hero_image_src = $hero_image[0];
			}

		}

	} else {

		// For the events page
		if ( is_post_type_archive( 'tribe_events' ) || is_singular( 'tribe_events' ) ) {
			if ( ( $hero_image = wp_get_attachment_image_src( get_option( 'options_events_hero_image' ), 'sa-hero-inside-page' ) )
				&& isset( $hero_image[0] ) ) {
				$hero_image_src = $hero_image[0];
			}
		}

		// For inside pages
		else if ( isset( $post->ID ) ) {
			$hero_image_id = get_post_meta( $post->ID, '_sa_hero_image', true );
		}

	}

	// Filter the image ID
	$hero_image_id = apply_filters( 'sa_framework_hero_image_id', $hero_image_id );

	// Filter the image source
	$hero_image_src = apply_filters( 'sa_framework_hero_image_source', $hero_image_src );

	// If we have a hero image ID, set for loading
	if ( $hero_image_id > 0 ) {
		$load_hero_image_id = $hero_image_id;
	}

	// Print the image
	if ( $load_hero_image_id || $hero_image_src ) {

		// Set classes
		$hero_image_classes = array( 'has-image' );
		if ( $load_hero_image_id ) {
			$hero_image_classes[] = 'load-hero-image';
		}

		// Print image holder
		?><div id="sa-page-hero-image" class="<?php echo implode( ' ', $hero_image_classes ); ?>"<?php if ( $load_hero_image_id ) { echo ' data-id="' . $load_hero_image_id . '"'; } else if ( $hero_image_src ) { echo ' style="background-image:url(' . $hero_image_src . ');"'; } ?>></div><?php

	}

}, 1 );

// Add the section header
add_action( 'sa_framework_before_main_area', function() {
	global $post;

	// Not on the home page
	if ( is_front_page() ) {
		return;
	}

	// Get page section info
	$page_section_title = null;
	$page_section_post_id = 0;

	// If an archive page...
	if ( is_archive() ) {
		$page_section_title = post_type_archive_title( '', false );
	}

	// For normal pages
	else {

		// See if there's a parent

		// Get the section ID
		$page_section_post_id = wp_cache_get( 'page_section_post_id', 'sa_framework' );

		// Get the page section title
		$page_section_title = get_the_title( $page_section_post_id ? $page_section_post_id : null );

	}

	// Filter the section header
	$page_section_header = apply_filters( 'sa_framework_section_header', $page_section_title );

	// Make sure we have a header
	if ( ! $page_section_header ) {
		return false;
	}

	// Means its the page title
	$is_page_title = ! $page_section_post_id || ( isset( $post->ID ) && $page_section_post_id == $post->ID );

	// Get the section permalink if we're not on the page
	$page_section_permalink = $is_page_title ? false : get_permalink( $page_section_post_id );

	// Filter the section permalink
	$page_section_permalink = apply_filters( 'sa_framework_section_header_permalink', $page_section_permalink );

	// Print the section header
	?><div id="sa-section-header-wrapper">
		<div class="row">
			<div class="small-12 medium-12 columns">
				<div id="sa-section-header"<?php echo $page_section_permalink ? ' class="has-permalink"' : null; ?>><?php

					if ( $page_section_permalink ) {
						?><a href="<?php echo $page_section_permalink; ?>"><?php echo $page_section_header; ?></a><?php
					} else {

						if ( $is_page_title ) {
							?><h1><?php echo $page_section_header; ?></h1><?php
						} else {
							echo $page_section_header;
						}

					}

				?></div>
			</div>
		</div>
	</div><?php

}, 2 );

// Remove the page title on certain pages
add_filter( 'sa_framework_display_page_title', function( $display_page_title ) {
	global $post;

	// Remove on search results and 404s
	if ( IS_SA_SEARCH_PAGE || is_404() ) {
		return false;
	}

	// Get the section ID
	if ( ( $page_section_post_id = wp_cache_get( 'page_section_post_id', 'sa_framework' ) )
		&& isset( $post->ID ) && $page_section_post_id == $post->ID ) {
		return false;
	}

	return $display_page_title;
}, 1000 );

// Filter the post class
add_filter( 'post_class', function( $classes, $class, $post_id ) {

	// Add class if section header
	if ( ( $page_section_post_id = wp_cache_get( 'page_section_post_id', 'sa_framework' ) )
		&& $page_section_post_id == $post_id ) {
		$classes[] = 'is-sa-page-section';
	}

	return $classes;
}, 100, 3 );

// Add custom section headers
add_filter( 'sa_framework_section_header', function( $page_section_title ) {

	// For 404s
	if ( is_404() ) {
		return 'Page Not Found';
	}

	return $page_section_title;
});

// Decide whether or not to show breadcrumbs
add_filter( 'sa_framework_display_page_breadcrumbs', function( $display_page_breadcrumbs ) {

	// Do not display on 404s
	if ( is_404() ) {
		return false;
	}

	return $display_page_breadcrumbs;
});