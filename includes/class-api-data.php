<?php

// Our data API class
class SA_Child_API_Data {

	/**
	 * Holds the class instance.
	 *
	 * @var		SA_Child_API_Data
	 */
	private static $instance;

	/**
	 * Returns the instance of this class.
	 *
	 * @return	SA_Child_API_Data
	 */
	public static function instance() {
		if ( ! isset( static::$instance ) ) {
			$className = __CLASS__;
			static::$instance = new $className;
		}
		return static::$instance;
	}

	/**
	 * Warm things up.
	 */
	protected function __construct() {}

	/**
	 * Method to keep our instance from being cloned.
	 *
	 * @since	1.0.0
	 * @access	private
	 * @return	void
	 */
	private function __clone() {}

	/**
	 * Method to keep our instance from being unserialized.
	 *
	 * @since	1.0.0
	 * @access	private
	 * @return	void
	 */
	private function __wakeup() {}

}

/**
 * Returns the instance of our main SA_Child_API_Data class.
 *
 * Will come in handy when we need to access the
 * class to retrieve data.
 *
 * @return	SA_Child_API_Data
 */
function sa_child_api_data() {
	return SA_Child_API_Data::instance();
}

// Let's get this show on the road
sa_child_api_data();