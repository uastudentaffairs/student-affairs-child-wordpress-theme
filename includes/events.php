<?php

// Remove the main loop on the events page
add_action( 'wp', function() {

	if ( is_post_type_archive( 'tribe_events' ) || is_singular( 'tribe_events' ) ) {

		// Remove the main loop
		remove_action( 'sa_framework_print_main_loop', 'sa_framework_print_main_loop' );

	}

}, 1000 );

// Filter the section header
add_filter( 'sa_framework_section_header', function( $page_section_title ) {
	return is_post_type_archive( 'tribe_events' ) || is_singular( 'tribe_events' ) ? 'Calendar of Events' : $page_section_title;
});

// Make sure events always have a post type archive title
add_filter( 'post_type_archive_title', function( $archive_title, $post_type ) {
	return ( 'tribe_events' == $post_type ) ? 'Calendar of Events' : $archive_title;
}, 100, 2 );

// Don't show the page title for events since we have the section header
add_filter( 'sa_framework_display_page_title', function( $display_page_title ) {
	return ( is_post_type_archive( 'tribe_events' ) || is_singular( 'tribe_events' ) ) ? false : $display_page_title;
}, 100 );

// Set the sidebar
add_filter( 'sa_child_left_sidebar_id', function( $left_sidebar_id ) {
	return is_singular( 'tribe_events' ) ? 'sa-single-event' : $left_sidebar_id;
});

// Remove the Tribe footer CSS
add_action( 'wp_footer', function() {
	remove_action( 'wp_print_footer_scripts', array( Tribe__Events__Pro__Customizer__Main::instance(), 'print_css_template' ), 15 );
});

// Print home page events section
function sa_child_print_front_page_events() {
	global $post;

	// Retrieves the posts used in the List Widget loop.
	$events = tribe_get_events( array(
		'eventDisplay'   => 'list',
		'posts_per_page' => 3,
	) );

	// Check if any posts were found.
	if ( isset( $events ) && $events ) :

		?><div id="events-wrapper">
			<div class="row">
				<div class="small-12 columns">
					<h2 class="section-title"><a href="<?php echo get_bloginfo( 'url' ); ?>/events/">Upcoming Events</a></h2>
					<div class="row"><?php

						foreach ( $events as $post ) :
							setup_postdata( $post );

							//$mini_cal_event_atts = tribe_events_get_widget_event_atts();
							$postDate = tribe_events_get_widget_event_post_date();

							?><div class="small-12 medium-4 columns end">
								<div class="event">
									<div class="event-date">
										<span class="list-dayname"><?php echo date_i18n( 'j', $postDate ); ?></span>
										<span class="list-daynumber"><?php echo date_i18n( 'M', $postDate ); ?></span>
									</div>

									<div class="list-info">
										<h3 class="event-title">
											<a href="<?php echo esc_url( tribe_get_event_link() ); ?>"
											   rel="bookmark"><?php the_title(); ?></a>
										</h3>
										<div class="event-duration">
											<?php echo tribe_events_event_schedule_details(); ?>
										</div>
										<?php the_excerpt(); ?>
									</div> <!-- .list-info -->

								</div><!-- .hentry .vevent -->
							</div><!--.columns--><?php

						endforeach;

						// Cleanup. Do not remove this.
						wp_reset_postdata();

					?></div><!--.row-->
				</div>
			</div>
		</div><?php

	endif;

}