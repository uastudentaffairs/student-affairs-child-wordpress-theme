<?php

// Template Name: SA Departments

// Add assets
add_action( 'wp_enqueue_scripts', function() {
	global $sa_theme_dir, $sa_framework_dir;

	// Enqueue the department script
	wp_enqueue_script( 'sa-child-departments', $sa_theme_dir . 'js/sa-child-departments.min.js', array( 'jquery', 'sa-child', 'handlebars', 'unveil' ) );

	// Pass some data
	wp_localize_script( 'sa-child-departments', 'sa_child_depts', array(
		'dir' => $sa_framework_dir
	));

}, 30 );

// Add departments after content
add_action( 'sa_framework_after_content', function() {

	// Get the search query
	$search_query = null;

	?><script id="sa-departments-template" type="text/x-handlebars-template">
		{{#.}}
			<li>
				<div class="sa-item has-bottom-details {{#grid_photo}}has-header-img{{/grid_photo}}">
					{{{header}}}
					{{#excerpt}}<div class="item-content">{{{rendered}}}</div>{{/excerpt}}
					<ul class="item-details item-bottom-details">
						{{#website}}<li class="has-icon has-a website"><a href="{{.}}"><span class="dashicons dashicons-admin-site"></span> <span class="a-label">{{.}}</span></a></li>{{/website}}
						{{#office}}<li class="has-icon office"><span class="dashicons dashicons-location"></span> {{.}}</li>{{/office}}
						{{#phone}}<li class="has-icon phone"><span class="dashicons dashicons-phone"></span> {{.}}</li>{{/phone}}
					</ul>
					{{{social_media_grid}}}
				</div>
			</li>
		{{/.}}
	</script>

	<script id="sa-departments-select-template" type="text/x-handlebars-template">
		{{#.}}
			<option value="{{link}}">{{title}}</option>
		{{/.}}
	</script>

	<form role="search" method="get" id="sa-departments-search-form" class="search-form search-bar<?php echo ! empty( $search_query ) ? ' active' : null; ?>" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<label for="sa-departments-select">
			<span class="screen-reader-text"><?php echo _x( 'Search our departments:', 'label' ); ?></span>
			<select id="sa-departments-select" class="select-field" title="Select one of our departments">
				<option value="">Select a department</option>
			</select>
		</label>
		<div class="search-field-wrapper">
			<label for="sa-departments-search-field"><span class="screen-reader-text"><?php echo _x( 'Search our departments:', 'label' ); ?></span></label>
			<input type="search" id="sa-departments-search-field" class="search-field" placeholder="Search our departments" value="<?php echo $search_query; ?>" name="s" title="Search our departments" />
			<div class="close-search-button"></div>
		</div>
		<input type="submit" class="button button-primary search-submit" autocomplete="off" value="Search" />
	</form>

	<ul id="sa-departments" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3 sa-items sa-items-grid sa-items-grid-match-height unveil" data-template="sa-departments-template"></ul><?php

});

get_header();

get_footer();