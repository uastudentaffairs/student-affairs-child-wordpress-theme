<?php

global $blog_id, $sa_theme_dir;

// Remove the default main area
remove_action( 'sa_framework_start_main_area', 'sa_framework_start_main_area' );

// Trim excerpt length
add_filter( 'excerpt_length', function($excerpt_length){
	return 20;
},100);


get_header();

// Get some blog info
$blog_url = get_bloginfo( 'url' );

// @TODO setup unveil on home page, especially social media

do_action( 'sa_framework_before_front_main' );

?><div id="sa-main"><?php

	// Main site
	if ( is_sa_main_site() ) {

		// Print tagline
		?><div id="sa-front-page-tagline-wrapper">
			<div class="row">
				<div class="small-12 columns">
					<div id="sa-front-page-tagline">The Division of Student Affairs <strong>maximizes each UA student's
							learning experiences</strong>.
					</div>
				</div>
			</div>
		</div><?php

	}

	// Print message
	if ( $sa_front_text_below_hero = get_option( 'options_sa_front_text_below_hero' ) ) {
		?><div id="sa-front-page-text">
			<div class="row">
				<div class="small-12 columns">
					<?php echo wpautop( $sa_front_text_below_hero ); ?>
				</div>
			</div>
		</div><?php
	}

	// Main site
	if ( is_sa_main_site() ) {


		// Print promo items
		?><script id="sa-front-promo-template" type="text/x-handlebars-template">
			<li>
				<div class="promo-item">
					{{header}}
					{{#content}}<div class="item-content">{{{rendered}}}</div>{{/content}}
				</div>
			</li>
		</script>
		<div id="sa-promo-wrapper">
			<div class="row">
				<div class="small-12 medium-6 large-7 columns">
					<ul id="sa-promo" class="small-block-grid-1 medium-block-grid-1 large-block-grid-2" data-count="4"></ul><?php

					/*if ( is_super_admin() ) {

						?><div id="otob-wrapper">
							<div class="row">
								<div class="small-12 columns">
									<div id="otob">
										<div class="otob-statement">"We serve with the entire Tuscaloosa community. We are One Team. One Bama."</div>
									</div>
								</div>
							</div>
						</div><?php

					}*/

				?></div>
				<div class="small-12 medium-6 large-5 columns">
					<div id="sa-stories"><?php

						// Print Capstone 101
						if ( ( $capstone_posts = get_posts( array( 'post_type' => 'stories', 'posts_per_page' => 1, 'meta_key' => 'story_type', 'meta_value' => 'c101' ) ) )
							&& ( $capstone_post = array_shift( $capstone_posts ) ) ) {

							?><div class="sa-story capstone-101">
								<span class="story-category">Capstone 101</span>
								<div class="story-header">
									<a href="<?php echo get_permalink( $capstone_post->ID ); ?>"><?php

										// Get the image
										$capstone_post_thumbnail_id = get_post_thumbnail_id($capstone_post->ID);
										if ( $capstone_post_thumbnail_id && ( $capstone_post_image_data = wp_get_attachment_image_src( $capstone_post_thumbnail_id, 'sa-stories-feature' ) ) ) {

											// Get the caption
											$caption = get_post_field( 'post_excerpt', $capstone_post_thumbnail_id );

											// Print the image
											?><img class="story-image" src="<?php echo $capstone_post_image_data[0]; ?>" alt="<?php echo $caption; ?>" /><?php

										}

										?><h2 class="story-title"><?php

											// Print the title
											echo get_the_title( $capstone_post->ID );

											// Add any subtitle
											if ( $subtitle = get_post_meta( $capstone_post->ID, 'subtitle', true ) ) {
												?> <span class="subtitle"><?php echo $subtitle; ?></span><?php
											}

										?></h2>
									</a>
								</div><?php

								// Print the excerpt
								$front_page_excerpt = get_post_meta( $capstone_post->ID, 'front_page_excerpt', true );
								if ( ! $front_page_excerpt ) {
									$front_page_excerpt = $capstone_post->post_excerpt;
								}
								if ( $front_page_excerpt ) {
									?><div class="story-content"><?php echo wpautop( wp_trim_words( $front_page_excerpt, 30, '...' ) ); ?></div><?php
								}

							?></div><?php

						}

						// Print Office Hours
						if ( ( $office_posts = get_posts( array( 'post_type' => 'stories', 'posts_per_page' => 1, 'meta_key' => 'story_type', 'meta_value' => 'office' ) ) )
						     && ( $office_post = array_shift( $office_posts ) ) ) {

							// Get the image
							$office_post_thumbnail_id = get_post_thumbnail_id( $office_post->ID );
							$office_post_caption = '';
							$office_post_image = '';
							if ( $office_post_thumbnail_id && ( $office_post_image = wp_get_attachment_thumb_url( $office_post_thumbnail_id ) ) ) {

								// Get the caption
								$office_post_caption = get_post_field( 'post_excerpt', $office_post_thumbnail_id );

							}

							?><div class="sa-story office-hours<?php echo $office_post_image ? ' has-side-image' : null; ?>">
								<span class="story-category">Office Hours</span>
								<div class="story-wrapper"><?php
									if ( $office_post_image ) {
										?><a href="<?php echo get_permalink( $office_post->ID ) ; ?>"><img class="story-image" src="<?php echo $office_post_image; ?>" alt="<?php echo $office_post_caption; ?>"/></a><?php
									}
									?><div class="story-header">
										<a href="<?php echo get_permalink( $office_post->ID ) ; ?>"><h2 class="story-title"><?php echo get_the_title( $office_post->ID ); ?></h2></a>
									</div><?php

									// Print the excerpt
									$front_page_excerpt = get_post_meta( $office_post->ID, 'front_page_excerpt', true );
									if ( ! $front_page_excerpt ) {
										$front_page_excerpt = $office_post->post_excerpt;
									}
									if ( $front_page_excerpt ) {
										?><div class="story-content"><?php echo wpautop( wp_trim_words( $front_page_excerpt, 30, '...' ) ); ?></div><?php
									}

								?></div>
							</div><?php

						}

						?><a class="more-stories" href="<?php echo $blog_url; ?>/our-stories/"><span class="a-label">More Stories</span> <span class="dashicons dashicons-arrow-right-alt2"></span></a>

					</div>
				</div>

			</div>
		</div><?php

		// Print events
		sa_child_print_front_page_events();

		// Get the social media grid
		if ( $social_media_grid = get_sa_social_media_grid() ) {

			?><div id="sa-social-media-grid-wrapper">
				<?php echo $social_media_grid; ?>
			</div><?php

		}

	} else {

		// Print promo items
		?><script id="sa-front-promo-template" type="text/x-handlebars-template">
			<li>
				<div class="promo-item">
					{{header}}
					{{#excerpt}}<div class="item-content">{{{rendered}}}</div>{{/excerpt}}
				</div>
			</li>
		</script>
		<div id="sa-promo-wrapper">
			<div class="row">
				<div class="small-12 columns">
					<ul id="sa-promo" class="small-block-grid-1 medium-block-grid-1 large-block-grid-3"></ul>
				</div>

			</div>
		</div><?php

		// Get social media accounts for grid
		$social_media_grid_accounts = array();

		// Count social media for buttons
		$count_social_media = 0;

		// Get Facebook info
		if ( $facebook_url = get_option( 'options_facebook_url' ) ) {
			$count_social_media++;
		}

		// Get Twitter info
		if ( $twitter_handle = get_option( 'options_twitter_handle' ) ) {
			$count_social_media++;

			// Add to grid
			$social_media_grid_accounts[ 'twitter' ][] = $twitter_handle;

		}

		// Get Instagram info
		if ( $instagram_handle = get_option( 'options_instagram_handle' ) ) {
			$count_social_media++;
		}
		// Add Instagram to grid
		if ( $instagram_user_id = get_option( 'options_instagram_user_id' ) ) {
			$social_media_grid_accounts[ 'instagram' ][] = $instagram_user_id;
		}

		if ( $count_social_media ) {

			// Build column classes
			$column_classes = array( 'small-12', 'columns' );

			if ( $count_social_media == 2 ) {
				$column_classes[] = 'medium-6';
			} else if ( $count_social_media == 3 ) {
				$column_classes[] = 'medium-4';
			}

			?><div id="sa-social-media-buttons">
				<div class="row"><?php

					if ( $facebook_url ) {
						?><div class="<?php echo implode( ' ', $column_classes ); ?>">
						<a class="social-media-button facebook has-icon" href="<?php echo $facebook_url; ?>"><span class="dashicons dashicons-facebook-alt"></span> Follow on Facebook</a>
						</div><?php
					}

					if ( $twitter_handle ) {
						?><div class="<?php echo implode( ' ', $column_classes ); ?>">
						<a class="social-media-button twitter has-icon" href="http://twitter.com/#!/<?php echo $twitter_handle; ?>"><span class="dashicons dashicons-twitter"></span> Follow on Twitter</a>
						</div><?php
					}

					if ( $instagram_handle ) {
						?><div class="<?php echo implode( ' ', $column_classes ); ?>">
						<a class="social-media-button instagram has-icon" href="https://www.instagram.com/<?php echo $instagram_handle; ?>"><span class="dashicons dashicons-instagram"></span> Follow on Instagram</a>
						</div><?php
					}

				?></div>
			</div><?php

		}

		// Career
		if ( 4 == $blog_id ) {

			// Add social media accounts
			$social_media_grid_accounts[ 'twitter' ][] = 'careerscba';

		}

		// Print events
		sa_child_print_front_page_events();

		// If main site, get everything
		if ( is_sa_main_site() ) {
			$social_media_grid_accounts = array( 'get_default_twitter' => true, 'get_default_instagram' => true );
		}

		// Get the social media grid
		if ( $social_media_grid = get_sa_social_media_grid( $social_media_grid_accounts ) ) {

			?><div id="sa-social-media-grid-wrapper">
				<?php echo $social_media_grid; ?>
			</div><?php

		}

	}

?></div> <!--#sa-main--><?php

do_action( 'sa_framework_after_front_main' );

get_footer();