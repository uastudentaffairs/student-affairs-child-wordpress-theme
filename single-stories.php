<?php

// Filter the title to add subtitle
add_filter( 'sa_framework_page_title', function( $title, $post_id ) {
	if ( $subtitle = get_post_meta( $post_id, 'subtitle', true ) ) {
		return '<span class="story-title">' . $title . '</span><span class="story-colon">:</span> <span class="story-subtitle">' . $subtitle . '</span>';
	}
	return $title;
}, 10, 2 );

add_action( 'sa_framework_before_page_title', function() {
	global $post, $sa_story_type;

	// Add story type
	if ( $sa_story_type = get_post_meta( $post->ID, 'story_type', true ) ) {

		// Set type label
		$sa_story_type_label = null;
		switch( $sa_story_type ) {
			case 'c101':
				$sa_story_type_label = 'Capstone 101';
				break;
			case 'office':
				$sa_story_type_label = 'Office Hours';
				break;
		}

		if ( $sa_story_type_label ) {
			?><span class="story-type"><?php echo $sa_story_type_label; ?></span><?php
		}
	}

});

add_action( 'sa_framework_after_page_title', function() {
	global $post, $sa_story_type;

	// Build story details
	$story_details = array(
		'published' => '<span class="story-detail story-published">Published ' . get_the_date( 'F j, Y', $post->ID ) . '</span>',
	);

	// Add byline
	if ( $byline = get_post_meta( $post->ID, 'byline', true ) ) {
		$story_details[ 'byline' ] = '<span class="story-detail story-byline">' . $byline . '</span>';
	}

	// Add byline notes
	if ( $byline_notes = get_post_meta( $post->ID, 'byline_notes', true ) ) {
		$story_details[ 'byline_notes' ] = '<span class="story-detail story-byline-notes">' . $byline_notes . '</span>';
	}

	// Print details
	if ( $story_details ) {
		?><span class="story-details"><?php echo implode( '', $story_details ); ?></span><?php
	}

	// Print featured image, but not for office hours
	if ( 'office' != $sa_story_type && ( $featured_image_id = get_post_thumbnail_id( $post->ID ) ) ) {
		if ( $feature_image_post = get_post( $featured_image_id ) ) {
			if ( $feature_image = wp_get_attachment_image_src( $featured_image_id, 'sa-stories-feature' ) ) {

				// Do we have a caption?
				$caption = isset( $feature_image_post->post_excerpt ) ? $feature_image_post->post_excerpt : null;

				// Build image
				$image_html = '<img src="' . $feature_image[ 0 ] . '" alt="" />';

				// Add caption
				if ( $caption ) {
					$image_html = '<div class="wp-caption">' . $image_html . '<p class="wp-caption-text">' . $caption . '</p></div>';
				}

				echo $image_html;

			}
		}
	}

});

get_header();

get_footer();