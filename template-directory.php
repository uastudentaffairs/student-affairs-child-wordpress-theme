<?php

// Template Name: SA Directory

// Show which directory?
global $sa_directory;
$sa_directory = is_page( 'people' ) ? 'people' : ( is_page( 'all' ) ? 'all' : 'departments' );

// Don't show breadcrumbs
remove_action( 'sa_framework_before_content_area', 'sa_framework_print_page_breadcrumbs' );

// Set the layout to full width
add_filter( 'sa_framework_page_layout', function( $defined_page_layout ) {
	return 'full-width-column';
});

// Don't include the section header permalink
add_filter( 'sa_framework_section_header_permalink', function( $page_section_permalink ) {
	return false;
}, 1000 );

// Don't show the page title
add_filter( 'sa_framework_display_page_title', function( $display_page_title ) {
	return false;
}, 1000 );

// Add scripts
add_action( 'wp_enqueue_scripts', function() {
	global $sa_directory, $sa_framework_dir, $sa_theme_dir;

	// Enqueue the departments assets
	if ( 'departments' == $sa_directory ) {

		// Enqueue the department script
		wp_enqueue_script( 'sa-child-departments', $sa_theme_dir . 'js/sa-child-departments.min.js', array( 'jquery', 'handlebars', 'unveil' ) );

		// Pass some data
		wp_localize_script( 'sa-child-departments', 'sa_child_depts', array(
			'dir' => $sa_framework_dir
		));

	}

	// Enqueue the people assets
	else if ( 'people' == $sa_directory ) {

		// Enqueue the people script
		wp_enqueue_script( 'sa-child-dir-people', $sa_theme_dir . 'js/sa-child-dir-people.min.js', array( 'jquery', 'handlebars', 'unveil' ) );

		// Pass some data
		wp_localize_script( 'sa-child-dir-people', 'sa_child_people', array(
			'themedir' => $sa_theme_dir
		));

	}

}, 30 );

// Add directory after content
add_action( 'sa_framework_after_content', function() {
	global $sa_directory, $sa_theme_dir;

	// Get the search query
	$search_query = null;

	// Get bloginfo
	$directory_url = trailingslashit( get_bloginfo( 'url' ) ) . 'directory/';

	?><ul class="tabs sa-directory-tabs">
		<li class="tab-title<?php echo 'departments' == $sa_directory ? ' active' : null; ?>" role="presentation"><a href="<?php echo $directory_url; ?>departments/" role="tab" tabindex="0" aria-selected="true">Departments</a></li>
		<li class="tab-title<?php echo 'people' == $sa_directory ? ' active' : null; ?>" role="presentation"><a href="<?php echo $directory_url; ?>people/" role="tab" tabindex="0" aria-selected="false">People</a></li>
		<?php /*<li class="tab-title<?php echo 'all' == $sa_directory ? ' active' : null; ?>" role="presentation"><a href="<?php echo $directory_url; ?>all/" role="tab" tabindex="0" aria-selected="false">A-Z</a></li>*/ ?>
	</ul><?php

	switch( $sa_directory ) {

		case 'departments':

			// Get the search query
			$search_query = null;

			?><script id="sa-departments-template" type="text/x-handlebars-template">
				{{#.}}
					<li>
						<div class="sa-item department">
							{{{simpleHeader}}}
							{{{item_details}}}
						</div>
					</li>
				{{/.}}
			</script>

			<script id="sa-departments-select-template" type="text/x-handlebars-template">
				{{#.}}
				<option value="{{link}}">{{title}}</option>
				{{/.}}
			</script>

			<form role="search" method="get" id="sa-departments-search-form" class="search-form search-bar<?php echo ! empty( $search_query ) ? ' active' : null; ?>" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<label for="sa-departments-select">
					<span class="screen-reader-text"><?php echo _x( 'Search our departments:', 'label' ); ?></span>
					<select id="sa-departments-select" class="select-field" title="Select one of our departments">
						<option value="">Select a department</option>
					</select>
				</label>
				<div class="search-field-wrapper">
					<label for="sa-departments-search-field"><span class="screen-reader-text"><?php echo _x( 'Search our departments:', 'label' ); ?></span></label>
					<input type="search" id="sa-departments-search-field" class="search-field" placeholder="Search our departments" value="<?php echo $search_query; ?>" name="s" title="Search our departments" />
					<div class="close-search-button"></div>
				</div>
				<input type="submit" class="button button-primary search-submit" autocomplete="off" value="Search" />
			</form>

			<ul id="sa-departments" class="small-block-grid-1 medium-block-grid-2 sa-items sa-items-grid-match-height unveil" data-template="sa-departments-template" data-columns="2"></ul><?php

			break;

		case 'people':

			// Do we want a specific department?
			$people_dept = get_query_var( 'people_dept' );

			?><script id="sa-people-template" type="text/x-handlebars-template">
				{{#.}}
					<li>
						<div class="sa-item person">
							{{{thumbnail}}}
							{{#title}}<h2 class="item-title">{{{rendered}}}</h2>{{/title}}
							{{#position}}<span class="person-position">{{.}}</span>{{/position}}
							{{#departments}}<span class="person-departments">{{{.}}}</span>{{/departments}}
							<ul class="person-details">
								{{#email}}<li class="person-detail person-email"><a href="mailto:{{.}}" class="has-icon"><span class="dashicons dashicons-email-alt"></span> <span>{{.}}</span></a></li>{{/email}}
								{{#phone}}<li class="person-detail person-phone"><span class="dashicons dashicons-phone"></span> {{.}}</li>{{/phone}}
								{{#office}}<li class="person-detail person-location"><span class="dashicons dashicons-location"></span> {{.}}</li>{{/office}}
								{{#box}}<li class="person-detail person-box">Box {{.}}</li>{{/box}}
							</ul>
						</div>
					</li>
				{{/.}}

			</script>

			<script id="sa-people-depts-select-template" type="text/x-handlebars-template">
				{{#.}}
					<option value="{{slug}}">{{title}}</option>
				{{/.}}
			</script>

			<form role="search" method="get" id="sa-people-search-form" class="search-form search-bar<?php echo ! empty( $search_query ) ? ' active' : null; ?>" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<label for="sa-people-depts-select">
					<span class="screen-reader-text"><?php echo _x( 'Search our staff directory:', 'label' ); ?></span>
					<select id="sa-people-depts-select" class="select-field" title="Select one of our departments">
						<option value="">Select a department</option>
					</select>
				</label>
				<div class="search-field-wrapper">
					<label for="sa-people-search-field"><span class="screen-reader-text"><?php echo _x( 'Search our staff directory:', 'label' ); ?></span></label>
					<input type="search" id="sa-people-search-field" class="search-field" placeholder="Search our staff directory" value="<?php echo $search_query; ?>" name="s" title="Search our staff directory"/>
					<div class="close-search-button"></div>
				</div>
				<input type="submit" class="button button-primary search-submit" autocomplete="off" value="Search"/>
			</form>

			<ul id="sa-people" class="small-block-grid-1 medium-block-grid-2 sa-items unveil" data-template="sa-people-template"<?php echo $people_dept ? ' data-department="' . $people_dept . '"' : null; ?>></ul>

			<div id="sa-people-load-more" class="button secondary">Load More</div><?php

			break;

	}

});

get_header();

get_footer();