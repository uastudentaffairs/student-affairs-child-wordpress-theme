<?php

// Template Name: SA People Listing

// Add scripts
add_action( 'wp_enqueue_scripts', function() {
	global $sa_theme_dir;

	// Enqueue the people listing script
	wp_enqueue_script( 'sa-child-people-listing', $sa_theme_dir . 'js/sa-child-people-listing.min.js', array( 'jquery', 'handlebars', 'unveil' ) );

}, 30 );

// Add listing after content
// @TODO change to after !!!!!!!!
add_action( 'sa_framework_after_content', function() {
	global $sa_theme_dir, $post;

	// Do we want a specific department?
	$departments = get_post_meta( $post->ID, '_sa_people_listing_department', true );

	// Do we want student workers?
	$students = get_post_meta( $post->ID, '_sa_people_listing_students', true );

	// Do we want to orderby affiliation?
	$orderby = get_post_meta( $post->ID, '_sa_people_listing_orderby', true );

	?><script id="sa-people-listing-template" type="text/x-handlebars-template">
		{{#.}}
		<div class="sa-item person{{#affiliation}} {{.}}{{/affiliation}}">
			<img class="thumbnail" alt="{{#title}}{{{rendered}}}{{/title}}" src="<?php echo $sa_theme_dir; ?>images/no-person.png" data-src="{{#thumbnail}}{{.}}{{/thumbnail}}{{^thumbnail}}<?php echo $sa_theme_dir; ?>images/no-person.png{{/thumbnail}}" />
			{{#title}}<h2 class="item-title">{{{rendered}}}</h2>{{/title}}
			{{#affiliation_label}}<li class="person-affiliation">{{.}}</li>{{/affiliation_label}}
			{{#position}}<li class="person-position">{{.}}</li>{{/position}}
			{{#departments}}<li class="person-departments">{{{.}}}</li>{{/departments}}
			{{{person_details}}}
			{{#content}}<div class="person-content">{{{rendered}}}</div>{{/content}}
		</div>
		{{/.}}
	</script>
	<div id="sa-people-listing" class="sa-items unveil" data-template="sa-people-listing-template"<?php echo ! empty( $orderby ) ? ' data-orderby="' . $orderby . '"' : null; echo ( $students > 0 ) ? ' data-students="1"' : null; echo $departments ? ( ' data-departments="' . ( is_array( $departments ) ? implode( ',', $departments ) : $departments ) . '"' ) : null; ?>></div><?php

});

get_header();

get_footer();