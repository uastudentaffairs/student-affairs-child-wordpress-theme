(function( $ ) {
    'use strict';

    // Will hold the templates
    var $people_templ = false;
    var $people_depts_select_templ = false;

    // Will hold the selected department
    var $selected_people_dept = null;

    // Will hold the people
    var $sa_people = null;

    // Will hold the last successful people query
    var $sa_people_last_query = false;

    // Will hold the search field
    var $search_field = false;

    // Will hold the load more field
    var $load_more_people = false;
    var $load_more_people_page = 1;

    // When the document is ready...
    $(document).ready(function() {

        // Get the people
        $sa_people = $( '#sa-people' );

        // Get the load more field
        $load_more_people = $( '#sa-people-load-more' );

        // Is there a selected department?
        var $pass_the_people_dept = $sa_people.data('department');
        if ( $pass_the_people_dept != undefined && $pass_the_people_dept != '' ) {
            $selected_people_dept = $pass_the_people_dept;
        }

        // Make sure we have a template
        if ( $sa_people.data( 'template' ) ) {

            // Get the templates
            var $people_templ_content = $('#' + $sa_people.data('template')).html();
            if ( $people_templ_content !== undefined && $people_templ_content != '' ) {

                // Parse the template
                $people_templ = Handlebars.compile( $people_templ_content ); // optional, speeds up future uses

                // Render the people
                render_sa_people();

            }

        }

        // Render templates for select
        var $people_depts_select_templ_content = $( '#sa-people-depts-select-template' ).html();
        $people_depts_select_templ = Handlebars.compile( $people_depts_select_templ_content ); // optional, speeds up future uses

        // Load departments
        render_sa_people_depts();

        // The search field
        $search_field = $( '#sa-people-search-field' );

        // Setup the timer
        var $search_typing_timer;
        var $search_typing_interval = 700; // time in ms

        // On keyup, start the countdown
        $search_field.on( 'keyup', function () {
            clearTimeout( $search_typing_timer );
            $search_typing_timer = setTimeout( sa_people_search_done_typing, $search_typing_interval );
            check_sa_people_search_status();
        });

        // On keydown, clear the countdown
        $search_field.on( 'keydown', function () {
            clearTimeout( $search_typing_timer );
            check_sa_people_search_status();
        });

        // Handle search
        $search_field.on( 'change paste propertychange', function($event) {

            // Update the people
            render_sa_people( 'filter[s]=' + $(this).val() );

        });

        // Halt the form
        $( '#sa-people-search-form' ).on( 'submit', function() {

            // Update the people
            render_sa_people( 'filter[s]=' + $(this).val() );

            return false;
        });

        // Close active search
        $( '#sa-people-search-form .close-search-button' ).on( 'click', function() {

            // Clear the search field
            $search_field.val('');

            // Update the people and status
            check_sa_people_search_status();
            render_sa_people();

        });

        // Handle "load more"
        $load_more_people.on( 'click', function() {

            // Load more people
            load_more_sa_people();

            return false;
        });

    });

    ///// FUNCTIONS /////

    // Checks to see if search is active
    function check_sa_people_search_status() {

        if ( $search_field.val() != '' ) {
            $('#sa-people-search-form').addClass( 'active' );
        } else {
            $('#sa-people-search-form').removeClass( 'active' );
        }

    }

    // User looks to be finished typing
    function sa_people_search_done_typing () {

        // Don't update if less than 2 characters
        if ( $search_field.val().length > 0 && $search_field.val().length < 2 ) {
            return false;
        }

        // Update the people
        render_sa_people( 'filter[s]=' + $search_field.val() );

    }

    // Get the departments
    function render_sa_people_depts() {

        // Define the select
        var $people_depts_select = $( '#sa-people-depts-select' );

        // Build query string
        // &filter[orderby]=title&filter[order]=asc
        var $query_string = 'filter[posts_per_page]=-1';

        // Get departments
        $.ajax( {
            url: '/wp-json/wp/v2/departments?' + $query_string,
            success: function ( $departments ) {

                // Render the templates
                var $rendered_select = $people_depts_select_templ( $departments );

                // Add to the list
                $people_depts_select.append( $rendered_select );

                // If we have a selected dept, then select it
                if ( $selected_people_dept ) {
                    $people_depts_select.find( 'option[value="' + $selected_people_dept + '"]' ).prop('selected',true);
                }

                // Handle select
                $people_depts_select.on( 'change', function() {

                    // Store the department
                    $selected_people_dept = $(this).val();

                    // Update the people
                    render_sa_people();

                    return false;

                });

            },
            cache: true
        } );

    }

    // Load more people
    function load_more_sa_people() {

        // Hide the load more
        $load_more_people.hide();

        // Add to load page
        $load_more_people_page++;

        // Build query string
        var $query_string = $sa_people_last_query;

        // Add page
        if ( $load_more_people_page > 1 ) {
            $query_string += '&page=' + $load_more_people_page;
        }

        // Get people
        $.ajax( {
            url: '/wp-json/wp/v2/people?' + $query_string,
            success: function ( $people, $text_status, $xhr ) {

                // Get total found posts (not just those on page)
                var $total_people = $xhr.getResponseHeader( 'X-WP-Total' );

                // Render the templates
                var $rendered_list = $people_templ( $people );

                // Add to the list
                $sa_people.append( $rendered_list );

                // Show the load more if needed
                if ( $sa_people.children().length < $total_people ) {
                    $load_more_people.show();
                }

                // Unveil the images
                $sa_people.find( 'img' ).unveil( 0, function() {
                    $(this).load(function() {
                        $(this).closest('.sa-item').css({'opacity':'1'});
                    });
                });
                $sa_people.find( 'img:in-viewport' ).trigger( 'unveil' );

                // Load those without an image
                $sa_people.find( "img[data-src='']" ).closest('.sa-item').css({'opacity':'1'});

            },
            cache: true
        } );

    }

    // Get/update the people
    function render_sa_people( $add_to_query_string ) {

        // Hide the load more
        $load_more_people.hide();

        // Build query string
        // &filter[orderby]=title&filter[order]=asc
        var $query_string = 'filter[posts_per_page]=20&filter[order]=ASC';

        // Add search
        if ( $add_to_query_string !== undefined && $add_to_query_string != '' ) {
            $query_string += '&' + $add_to_query_string;
        }

        // Add departments
        if ( $selected_people_dept != '' && $selected_people_dept !== null ) {
            $query_string += '&filter[departments]=' + $selected_people_dept;
        }

        // Get people
        $.ajax( {
            url: '/wp-json/wp/v2/people?' + $query_string,
            success: function ( $people, $text_status, $xhr ) {

                // Get total found posts (not just those on page)
                var $total_people = $xhr.getResponseHeader( 'X-WP-Total' );

                // Store the query
                $sa_people_last_query = $query_string;

                // Render the templates
                var $rendered_list = $people_templ( $people );

                // Add to the list
                $sa_people.html( $rendered_list );

                // Show the load more if needed
                if ( $sa_people.children().length < $total_people ) {
                    $load_more_people.show();
                }

                // Unveil the images
                $sa_people.find( 'img' ).unveil( 0, function() {
                    $(this).load(function() {
                        $(this).closest('.sa-item').css({'opacity':'1'});
                    });
                });
                $sa_people.find( 'img:in-viewport' ).trigger( 'unveil' );

                // Load those without an image
                $sa_people.find( "img[data-src='']" ).closest('.sa-item').css({'opacity':'1'});

            },
            cache: true
        } );

    }

    // Format the header
    Handlebars.registerHelper( 'thumbnail', function( $options ) {

        // Build the thumbnail
        var $thumbnail = '<img class="thumbnail"';

        // Add name as alt text
        var $alt_text = ( this.title.rendered !== undefined && this.title.rendered != '' ) ? this.title.rendered : null;
        if ( $alt_text ) {
            $thumbnail += ' alt="' + $alt_text + '"';
        }

        // Add default source
        $thumbnail += ' src="' + sa_child_people.themedir + 'images/no-person.png"';

        // Add the data src for unveiling
        $thumbnail += ' data-src="';

        // If we have a thumbnail,
        if ( this.thumbnail !== undefined && this.thumbnail != '' ) {
            $thumbnail += this.thumbnail;
        }

        // Otherwise, add
         else {
            $thumbnail += sa_child_people.themedir + 'images/no-person.png';
        }

        // Close the thumbnail
        $thumbnail += '" />';

        // Return
        return new Handlebars.SafeString($thumbnail);

    });

})( jQuery );