(function( $ ) {
    'use strict';

    // Will hold the templates
    var $departments_templ = false;
    var $departments_select_templ = false;

    // We only want to load the departments select once so is true once loaded
    var $departments_select_loaded = false;

    // Will hold the departments
    var $sa_departments = false;

    // Will hold the search field
    var $search_field = false;

    // When the document is ready...
    $(document).ready(function() {

        // Define the departments
        $sa_departments = $( '#sa-departments' );

        // Make sure we have a template
        if ( $sa_departments.data( 'template' ) ) {

            // Get the templates
            var $departments_templ_content = $('#' + $sa_departments.data('template')).html();
            if ( $departments_templ_content !== undefined && $departments_templ_content != '' ) {

                // Parse the template
                $departments_templ = Handlebars.compile( $departments_templ_content );

                // Render the departments
                render_sa_departments();

            }

        }

        // Render templates for select
        var $departments_select_templ_content = $( '#sa-departments-select-template' ).html();
        if ( $departments_select_templ_content !== undefined && $departments_select_templ_content != '' ) {
            $departments_select_templ = Handlebars.compile($departments_select_templ_content); // optional, speeds up future uses
        }

        // The search field
        $search_field = $( '#sa-departments-search-field' );

        // Setup the timer
        var $search_typing_timer;
        var $search_typing_interval = 700; // time in ms

        // On keyup, start the countdown
        $search_field.on( 'keyup', function () {
            clearTimeout( $search_typing_timer );
            $search_typing_timer = setTimeout( sa_department_search_done_typing, $search_typing_interval );
            check_sa_department_search_status();
        });

        // On keydown, clear the countdown
        $search_field.on( 'keydown', function () {
            clearTimeout( $search_typing_timer );
            check_sa_department_search_status();
        });

        // Handle search
        $search_field.on( 'change paste propertychange', function($event) {

            // Update the departments
            render_sa_departments( $(this).val() );

        });

        // Halt the form
        $( '#sa-departments-search-form').on( 'submit', function() {

            // Update the departments
            render_sa_departments( $(this).val() );

            return false;
        });

        // Close active search
        $( '#sa-departments-search-form .close-search-button' ).on( 'click', function() {

            // Clear the search field
            $search_field.val('');

            // Update the departments and status
            check_sa_department_search_status();
            render_sa_departments();

        });

    });

    ///// FUNCTIONS /////

    // Checks to see if search is active
    function check_sa_department_search_status() {

        if ( $search_field.val() != '' ) {
            $('#sa-departments-search-form').addClass( 'active' );
        } else {
            $('#sa-departments-search-form').removeClass( 'active' );
        }

    }

    // User looks to be finished typing
    function sa_department_search_done_typing () {

        // Don't update if less than 2 characters
        if ( $search_field.val().length > 0 && $search_field.val().length < 2 ) {
            return false;
        }

        // Update the departments
        render_sa_departments( $search_field.val() );

    }

    // Get/update the departments
    function render_sa_departments( $search ) {

        // Build query string
        // &filter[orderby]=title&filter[order]=asc
        var $query_string = 'filter[posts_per_page]=-1';

        // Add search
        if ( $search !== undefined && $search != '' ) {
            $query_string += '&filter[s]=' + $search;
        }

        // Get departments
        $.ajax( {
            url: '/wp-json/wp/v2/departments?' + $query_string,
            success: function ( $departments ) {

                // Render the templates
                var $rendered_grid = $departments_templ( $departments );

                // Add to the grid
                $sa_departments.html( $rendered_grid );

                //  Match height
                if ( $sa_departments.hasClass( 'sa-items-grid-match-height' ) ) {
                    $sa_departments.sa_child_match_archive_grid_height();
                }

                // Unveil the images
                $sa_departments.find( 'img' ).unveil( 0, function() {
                    $(this).load(function() {
                        $(this).closest('.sa-item').css({'opacity':'1'});
                    });
                });
                $sa_departments.find( 'img:in-viewport' ).trigger( 'unveil' );

                // Load those without an image
                $sa_departments.find( "img[data-src='']" ).closest('.sa-item').css({'opacity':'1'});

                // Only load the select once
                if ( ! $departments_select_loaded ) {

                    // Don't load anymore
                    $departments_select_loaded = true;

                    // Render the templates
                    var $rendered_select = $departments_select_templ( $departments );

                    // Add to the grid and handle select
                    $( '#sa-departments-select' ).append( $rendered_select).on( 'change', function() {

                        // Redirect to URL
                        if( $(this).val() != '' ) {
                            window.location.href = $(this).val();
                        }

                        return false;

                    });

                }

            },
            cache: true
        } );

    }

    // Format the header
    Handlebars.registerHelper( 'header', function( $options ) {

        // Build the header
        var $header = '';

        // Add the grid photo
        if ( this.grid_photo !== undefined && this.grid_photo != '' ) {
            $header += '<img src="' + sa_child_depts.dir + 'images/transp/transp-600x200.png" data-src="' + this.grid_photo + '" />';
        }

        // Add the title
        if ( this.title.rendered !== undefined && this.title.rendered != '' ) {
            $header += '<h2 class="item-title">' + this.title.rendered + '</h2>';
        }

        // Wrap in the link
        if ( this.link !== undefined && this.link != '' ) {
            $header = '<a href="' + this.link + '">' + $header + '</a>';
        }

        // Wrap in the URL
        if ( $header ) {
            return new Handlebars.SafeString('<div class="item-header">'+$header+'</div>');
        }

        return null;
    });

    // Format the header
    Handlebars.registerHelper( 'simpleHeader', function( $options ) {


        // Add the title
        if ( this.title !== undefined && this.title != '' ) {

            // Add the title
            var $header = '';

            // Add the title
            if ( this.title.rendered !== undefined && this.title.rendered != '' ) {
                $header += this.title.rendered;
            }

            // Wrap in the link
            if ( this.link !== undefined && this.link != '' ) {
                $header = '<a href="' + this.link + '">' + $header + '</a>';
            }

            // Wrap in h2
            $header = '<h2 class="item-title">' + $header + '</h2>';

            // Return
            return new Handlebars.SafeString($header);

        }

        return null;
    });

    // Format the social media grid
    Handlebars.registerHelper( 'social_media_grid', function( $options ) {

        // Build social media grid
        var $grid = '';
        var $grid_item_count = 0;

        // Get twitter
        if ( this.twitter_handle !== undefined && this.twitter_handle != '' ) {
            $grid += '<li class="twitter"><a href="https://twitter.com/' + this.twitter_handle + '"><span class="dashicons dashicons-twitter"></span> <span class="screen-reader-text">@' + this.twitter_handle + '</span></a></li>';
            $grid_item_count++;
        }

        // Get facebook
        if ( this.facebook_url !== undefined && this.facebook_url != '' ) {
            $grid += '<li class="facebook"><a href="' + this.facebook_url + '"><span class="dashicons dashicons-facebook"></span> <span class="screen-reader-text">' + this.facebook_url + '</span></a></li>';
            $grid_item_count++;
        }

        // Get instagram
        if ( this.instagram_handle !== undefined && this.instagram_handle != '' ) {
            $grid += '<li class="instagram"><a href="https://www.instagram.com/' + this.instagram_handle + '"><span class="dashicons dashicons-instagram"></span> <span class="screen-reader-text">https://www.instagram.com/' + this.instagram_handle + '</span></a></li>';
            $grid_item_count++
        }

        // Wrap in the URL
        if ( $grid ) {
            return new Handlebars.SafeString('<ul class="sa-social-media-grid small-block-grid-' + $grid_item_count + '">' + $grid + '</ul>');
        }

        return null;
    });

    // Format the item details list
    Handlebars.registerHelper( 'item_details', function( $options ) {

        // Build items
        var $items = '';

        // Get website
        if ( this.website !== undefined && this.website != '' ) {
            $items += '<li class="has-icon website"><a href="' + this.website + '"><span class="dashicons dashicons-admin-site"></span> <span class="a-label">' + this.website + '</span></a></li>';
        }

        // Get office
        if ( this.office !== undefined && this.office != '' ) {
            $items += '<li class="has-icon office"><span class="dashicons dashicons-location"></span> ' + this.office + '</li>';
        }

        // Get phone
        if ( this.phone !== undefined && this.phone != '' ) {
            $items += '<li class="has-icon phone"><span class="dashicons dashicons-phone"></span> ' + this.phone + '</li>';
        }

        // Get twitter
        if ( this.twitter_handle !== undefined && this.twitter_handle != '' ) {
            $items += '<li class="has-icon twitter"><a href="https://twitter.com/' + this.twitter_handle + '"><span class="dashicons dashicons-twitter"></span> <span class="a-label">https://twitter.com/' + this.twitter_handle + '</span></a></li>';
        }

        // Get facebook
        if ( this.facebook_url !== undefined && this.facebook_url != '' ) {
            $items += '<li class="has-icon facebook"><a href="' + this.facebook_url + '"><span class="dashicons dashicons-facebook"></span> <span class="a-label">' + this.facebook_url + '</span></a></li>';
        }

        // Get instagram
        if ( this.instagram_handle !== undefined && this.instagram_handle != '' ) {
            $items += '<li class="has-icon instagram"><a href="https://www.instagram.com/' + this.instagram_handle + '"><span class="dashicons dashicons-instagram"></span> <span class="a-label">https://www.instagram.com/' + this.instagram_handle + '</span></a></li>';
        }

        // Wrap in the URL
        if ( $items ) {
            return new Handlebars.SafeString('<ul class="item-details">' + $items + '</ul>');
        }

        return null;
    });

})( jQuery );