(function( $ ) {
    'use strict';

    // When the document is ready...
    $(document).ready(function() {

        // If we have any archive grid templates
        if ( $( '.sa-items-grid-template').length > 0 ) {

            // Loop through each template
            $( '.sa-items-grid-template' ).each( function($index) {

                // Render the archive
                $(this).render_sa_archive();

            });

        }

        // If we have any grids who don't need loading but need height matches
        if ( $( '.sa-items-grid.sa-items-match-height:not(.sa-items-grid-template)').length > 0 ) {

            // Loop through each template
            $( '.sa-items-grid.sa-items-match-height:not(.sa-items-grid-template)' ).each( function($index) {
                $(this).sa_child_match_archive_grid_height();
            });

        }


    });

    ///// FUNCTIONS /////

    // Get/update the archive
    jQuery.fn.render_sa_archive = function() {

        // Make sure we have an API
        if ( ! $(this).data( 'api' ) ) {
            return false;
        }

        // Make sure we have a template
        if ( ! $(this).data( 'template' ) ) {
            return false;
        }

        // Make sure we have template content
        var $template_content = $( '#' + $(this).data( 'template' ) ).html();
        if ( $template_content === undefined || $template_content == '' ) {
            return false;
        }

        // Parse the template
        var $template = Handlebars.compile( $template_content ); // optional, speeds up future uses

        // Set the archive grid
        var $archive_grid = $(this);

        // Get the template
        var $template = $( '#' + $(this).data( 'template' ) ).html();

        // Get departments
        $.ajax( {
            url: '/wp-json/wp/v2/posts?' + $(this).data( 'api' ),
            success: function ( $content ) {

                // Render the templates
                var $rendered_grid = $template( $content );

                // Add to the grid
                $archive_grid.html( $rendered_grid );

                //  Match height
                $archive_grid.sa_child_match_archive_grid_height();

                // Unveil the images
                if ( $archive_grid.hasClass( 'unveil' ) ) {

                    $archive_grid.find('img').unveil(0, function () {
                        $(this).load(function () {
                            $(this).closest('.sa-item').css({'opacity': '1'});
                        });
                    });
                    $archive_grid.find('img:in-viewport').trigger('unveil');

                    // Load those without an image
                    $archive_grid.find("img[data-src='']").closest('.sa-item').css({'opacity': '1'});

                }

            },
            cache: true
        } );

    }

})( jQuery );