(function( $ ) {
    'use strict';

    // When the document is ready...
    $(document).ready(function() {

        // Find the first level menu items to edit
        $( '#menu-to-edit > .menu-item.menu-item-depth-0').each( function() {

            // Set the menu item
            var $menu_item = $(this);

            // Get the menu item ID
            var $menu_item_id = $menu_item.attr( 'id').replace( /^menu\-item\-/, '' );

            // Will hold menu item width
            var $menu_item_width = 0;

            // Get the menu item value
            $.ajax({
                method: 'POST',
                url: ajaxurl,
                async: true,
                data: {
                    action: 'sa_child_get_menu_width',
                    menu_item_id: $menu_item_id
                }
            }).success(function( $data ) {
                $menu_item_width = $data;
            }).complete(function() {

                // Make sure it has value
                if ( ! ( $menu_item_width > 0 ) ) {
                    $menu_item_width = '';
                }

                // Create the html for our custom width field
                var $custom_width_field = $( '<p class="field-width description description-wide"><label for="edit-menu-item-width-' + $menu_item_id + '"><strong>Menu Item Width (In Percentage)</strong><br/><input type="text" id="edit-menu-item-width-' + $menu_item_id + '" class="widefat code edit-menu-item-width" name="menu-item-width[' + $menu_item_id + ']" value="' + $menu_item_width + '" /><span class="description">Only include digits and periods. <strong>Do not</strong> include the percent sign. Keep in mind that the width percentages for all top-level menu items must equal 100.</span></label></p>' );

                // Style the area
                $custom_width_field.css({ 'background': '#ddd', 'padding': '10px', 'color': '#222', '-moz-box-sizing': 'border-box', '-webkit-box-sizing': 'border-box', 'box-sizing': 'border-box' });

                // Add after the link target
                $custom_width_field.insertAfter( $menu_item.find( '.field-link-target' ) );

            });

        });

    });

    ////// FUNCTIONS //////

})( jQuery );