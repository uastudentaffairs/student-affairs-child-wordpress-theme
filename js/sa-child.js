(function( $ ) {
    'use strict';

    // When the document is ready...
    $(document).ready(function() {

        // Load the hero image
        var $load_hero_image = $( '#sa-page-hero-image.load-hero-image' );
        if ( $load_hero_image.length > 0 ) {

            // Make sure we have an image ID
            if ( $load_hero_image.data( 'id' ) ) {

                // Get hero images
                $.ajax({
                    url: 'https://sa.ua.edu/wp-json/wp/v2/hero_images/' + $( '#sa-page-hero-image.load-hero-image').data( 'id' ),
                    success: function ( $hero_image ) {

                        // Make sure the hero image is defined
                        if ( $hero_image !== undefined || $hero_image.hero_image !== undefined && $hero_image.hero_image != '' ) {
                            $load_hero_image.hide().css({ 'background-image':'url(' + $hero_image.hero_image + ')' }).fadeIn( 700 );
                        } else {

                            // If no image, remove image holder
                            $load_hero_image.remove();

                        }

                    },
                    cache: true
                });

            } else {

                // If no image, remove image holder
                $load_hero_image.remove();

            }

        }

        // Setup the main menu dropdown
        /*if ( $( '#sa-main-menu li.menu-item-has-children:not(.no-sa-dropdown-menu)' ).length > 0 ) {

            // Get the dropdown objects
            var $main_menu_dropdown_wrapper = $( '#sa-main-menu-dropdown-wrapper' );
            var $main_menu_dropdown = $( '#sa-main-menu-dropdown' );

            // Handle the main dropdown menu
            $( '#sa-main-menu li.menu-item-has-children:not(.no-sa-dropdown-menu) > a' ).on( 'click', function( $event ) {
                $event.preventDefault();

                // Find parent
                var $submenu_parent = $(this).parent('li');

                // If open...
                if ( $submenu_parent.hasClass( 'dropdown-open' ) ) {

                    // Let us know it's closed
                    $submenu_parent.removeClass( 'dropdown-open' );

                    // Hide the dropdown
                    $main_menu_dropdown_wrapper.hide();

                    // Empty the dropdown
                    $main_menu_dropdown.empty();

                }

                // If not open...
                else {

                    // Take contents of submenu
                    var $submenu_items = $submenu_parent.children('.sub-menu').children('li');

                    // Empty the dropdown
                    $main_menu_dropdown.empty();

                    // Load into dropdown
                    $submenu_items.clone().appendTo($main_menu_dropdown);

                    // Create overview item
                    var $submenu_overview = $submenu_parent.clone().removeAttr('style').addClass('menu-item-overview');

                    // Change text
                    $submenu_overview.find('span').html('Overview');

                    // Add the overview item
                    $submenu_overview.prependTo($main_menu_dropdown);

                    // Show the dropdown
                    $main_menu_dropdown_wrapper.show();

                    // Let us know it's open
                    $submenu_parent.addClass( 'dropdown-open' );

                }

            });

        }*/

    });

    ///// FUNCTIONS /////
    // Handle matching heights for archive grid
    jQuery.fn.sa_child_match_archive_grid_height = function() {

        // Define the archive grid
        var $archive_grid = $(this);

        var $col = 0;
        var $col_count = $archive_grid.data('columns' ) !== undefined && $archive_grid.data('columns' ) > 0 ? $archive_grid.data('columns' ) : 3;
        var $get_col_height = true;
        var $col_height = 0;
        for ( var $item = 0; $item < $archive_grid.find( '.sa-item' ).length; $item++ ) {
            $col++;

            // Get the item
            var $this_item = $archive_grid.find( '.sa-item' ).eq( $item );

            // Get height
            if ( $get_col_height ) {

                // Get the item height
                var $this_height = $this_item.outerHeight();
                if ( $this_height > $col_height ) {
                    $col_height = $this_height;
                }

                // Reset column
                if ( $col_count == $col ) {
                    $get_col_height = false;
                    $item -= $col_count;
                    $col = 0;
                }

            }

            // Apply height
            else {

                // If we need to adjust height
                if ( $this_item.outerHeight() < $col_height ) {

                    // If we have item content
                    if ( $this_item.find('.item-content').length > 0 ) {
                        var $this_item_content = $this_item.find('.item-content');
                        $this_item_content.height( $this_item_content.outerHeight() + ( $col_height - $this_item.outerHeight() ) );
                    } else {
                        $this_item.css({'height': $col_height});
                    }

                }

                // Reset column
                if ( $col_count == $col ) {
                    $get_col_height = true;
                    $col = 0;
                    $col_height = 0;
                }

            }

        }

    }

})( jQuery );