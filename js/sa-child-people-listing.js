(function( $ ) {
    'use strict';

    // Will hold the templates
    var $people_templ = false;

    // When the document is ready...
    $(document).ready(function() {

        // Get the people
        var $sa_people_listing = $( '#sa-people-listing' );

        // Make sure we have a template
        if ( $sa_people_listing.data( 'template' ) ) {

            // Get the templates
            var $people_templ_content = $('#' + $sa_people_listing.data('template')).html();
            if ( $people_templ_content !== undefined && $people_templ_content != '' ) {

                // Parse the template
                $people_templ = Handlebars.compile( $people_templ_content );

                // Render the people
                $sa_people_listing.render_sa_people();

            }

        }

    });

    ///// FUNCTIONS /////

    // Get/update the people
    $.fn.render_sa_people = function() {

        // Get the people
        var $sa_people_listing = $(this);

        // Build query string
        var $query_string = '';

        // Is there a selected department?
        var $sa_people_departments = $sa_people_listing.data('departments');
        if ( $sa_people_departments != undefined && $sa_people_departments != '' ) {
            $query_string += '&filter[departments]=' + $sa_people_departments;
        }

        // Do we want student workers?
        var $sa_people_students = $sa_people_listing.data('students');
        if ( $sa_people_students != undefined && $sa_people_students > 0 ) {
            $query_string += '&filter[students]=1';
        }

        // Do we want to change the orderby?
        var $sa_people_orderby = $sa_people_listing.data('orderby');
        if ( $sa_people_orderby != undefined && $sa_people_orderby != '' ) {
            $query_string += '&filter[orderby]='+$sa_people_orderby;
        }

        /*// Add search
        if ( $add_to_query_string !== undefined && $add_to_query_string != '' ) {
            $query_string += '&' + $add_to_query_string;
        }*/

        // Get people
        $.ajax( {
            url: 'https://sa.ua.edu/wp-json/wp/v2/people?filter[posts_per_page]=-1' + $query_string,
            success: function ( $people ) {

                // Render the templates
                var $rendered_list = $people_templ( $people );

                // Add to the list
                $sa_people_listing.html( $rendered_list );

                // Unveil the images
                $sa_people_listing.find( 'img' ).unveil( 0, function() {
                    $(this).load(function() {
                        $(this).closest('.sa-item').css({'opacity':'1'});
                    });
                });
                $sa_people_listing.find( 'img:in-viewport' ).trigger( 'unveil' );

                // Load those without an image
                $sa_people_listing.find( "img[data-src='']" ).closest('.sa-item').css({'opacity':'1'});

            },
            cache: true
        } );

    }

    // Format the person details list
    Handlebars.registerHelper( 'person_details', function( $options ) {

        // Build items
        var $items = '';

        // Get email
        if ( this.email !== undefined && this.email != '' ) {
            $items += '<li class="person-detail person-email"><a href="mailto:' + this.email + '" class="has-icon"><span class="dashicons dashicons-email-alt"></span> <span>' + this.email + '</span></a></li>';
        }

        // Get phone
        if ( this.phone !== undefined && this.phone != '' ) {
            $items += '<li class="person-detail person-phone"><span class="dashicons dashicons-phone"></span> ' + this.phone + '</li>';
        }

        // Get office
        if ( this.office !== undefined && this.office != '' ) {
            $items += '<li class="person-detail person-location"><span class="dashicons dashicons-location"></span> ' + this.office + '</li>';
        }

        // Get box
        if ( this.box !== undefined && this.box != '' ) {
            $items += '<li class="person-detail person-box">Box ' + this.box + '</li>';
        }

        // Wrap in the URL
        if ( $items ) {
            return new Handlebars.SafeString('<ul class="person-details">' + $items + '</ul>');
        }

        return null;
    });

})( jQuery );