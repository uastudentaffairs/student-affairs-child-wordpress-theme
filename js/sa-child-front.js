(function( $ ) {
    'use strict';

    // When the document is ready...
    $(document).ready(function() {

        // Set the promo wrapper
        var $promo_wrapper = $( '#sa-promo-wrapper' );
        var $promo = $( '#sa-promo' );

        // Get the templates
        var $promo_template_content = $('#sa-front-promo-template').html();
        if ( $promo_template_content !== undefined && $promo_template_content ) {

            // Parse the template
            var $promo_templ = Handlebars.compile( $promo_template_content );

            // Get count
            var item_count = $promo.data('count') !== undefined && $promo.data('count') > 0 ? $promo.data('count') : 3;

            // Get/setup the promos
            $.ajax( {
                url: '/wp-json/wp/v2/promos?filter[posts_per_page]=' + item_count + '&filter[orderby]=menu_order&filter[order]=ASC',
                success: function ( $promo_items ) {

                    // Build the HTML
                    var $promo_html = '';

                    // Go through each item
                    $.each( $promo_items, function( $index, $item ) {

                        // Make sure its active
                        if ( $item.promo_status == 'inactive' ) {
                            return true;
                        }

                        // Make sure it has an image
                        if ( $item.promo_featured_image == undefined || $item.promo_featured_image == '' ) {
                            return true;
                        }

                        // Render the templates
                        $promo_html += $promo_templ($item);

                    });

                    // Add the html
                    if ( $promo_html != '' ) {
                        $promo.html($promo_html);
                    }

                    // Remove everything
                    else {
                        $promo_wrapper.remove();
                    }

                }

            } );

        }

    });

    // Format the header
    Handlebars.registerHelper( 'header', function( $options ) {

        // Get the title
        var $item_header = this.title.rendered;

        // Wrap in the URL
        if ( $item_header !== undefined && $item_header ) {

            // Wrap in <h2>
            $item_header = '<h2 class="item-title">' + $item_header + '</h2>';

            // Get the featured image
            if ( this.promo_featured_image != undefined && this.promo_featured_image != '' ) {
                $item_header = '<img class="item-image" src="' + this.promo_featured_image + '" alt="" />' + $item_header;
            }

            // Wrap in link
            if ( this.promo_link !== undefined && this.promo_link != '' ) {

                // Build the link
                var $link = '<a href="' + this.promo_link + '"';

                // Get the target
                if ( this.promo_link_target !== undefined && this.promo_link_target != '' ) {
                    $link += ' target="' + this.promo_link_target + '"';
                }

                // Finish the link
                $link += '>';

                // Wrap around the header
                $item_header = $link + $item_header + '</a>';
            }

            return new Handlebars.SafeString('<div class="item-header">' + $item_header + '</div>');

        }
        return null;
    });

})( jQuery );