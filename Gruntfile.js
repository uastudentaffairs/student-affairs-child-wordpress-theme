module.exports = function(grunt) {

    grunt.initConfig({
        sass: {
            sachild: {
                options: {
                    sourcemap: 'none',
                    style: 'compressed',
                    noCache: true,
                    update: false,
                    loadPath: '../themes/student-affairs-framework/scss'
                },
                files: [{
                    expand: true,
                    src: '*.scss',
                    cwd: 'scss',
                    dest: 'css',
                    ext: '.min.css'
                }]
            },
        },
        uglify: {
            options: {
                mangle: false,
                compress: false
            },
            sachild: {
                files: [{
                    expand: true,
                    src: [ '*.js', '!*.min.js' ],
                    cwd: 'js',
                    dest: 'js',
                    ext: '.min.js'
                }]
            }
        },
        watch: {
            sachildjs: {
                files: [ 'js/*.js', '!*.min.js' ],
                tasks: [ 'newer:uglify:sachild' ]
            },
            sachildsass: {
                files: [ 'scss/*.scss' ],
                tasks: [ 'newer:sass:sachild' ]
            },
            sachildsasspartials: {
                files: [ 'scss/\_*.scss' ],
                tasks: [ 'sass:sachild' ]
            }
        }
    });

    // Load our dependencies
    grunt.loadNpmTasks( 'grunt-contrib-sass' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-watch' );
    grunt.loadNpmTasks( 'grunt-newer' );

    // Register our tasks
   	grunt.registerTask( 'default', [ 'newer:sass', 'newer:uglify', 'watch' ] );

    // Register a watch function
    grunt.event.on( 'watch', function( action, filepath, target ) {
        grunt.log.writeln( target + ': ' + filepath + ' has ' + action );
    });

};