<?php

// Get directory
global $sa_theme_dir;
$sa_theme_dir = trailingslashit( get_stylesheet_directory_uri() );

// Include files
require_once( STYLESHEETPATH . '/includes/filters.php' );
require_once( STYLESHEETPATH . '/includes/social-media.php' );
require_once( STYLESHEETPATH . '/includes/events.php' );
require_once( STYLESHEETPATH . '/includes/stories.php' );
require_once( STYLESHEETPATH . '/includes/shortcodes.php' );

// Add admin functionality in admin
if( is_admin() ) {
    require_once( STYLESHEETPATH . '/includes/admin.php' );
}

// Add image sizes
add_action( 'after_setup_theme', function() {
    add_image_size( 'sa-hero-front-page', 2000, 600, true );
    add_image_size( 'sa-hero-inside-page', 2000, 300, true );
    add_image_size( 'sa-stories-feature', 900, 500, true );
    add_image_size( 'sa-archive-block-grid', 600, 220, true );
	add_image_size( 'sa-front-promo', 620, 260, true );
}, 10 );

// Register sidebars
add_action( 'widgets_init', function() {

    register_sidebar( array(
        'name'          => 'Main Sidebar',
        'id'            => 'sa-main',
        'description'   => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s sidebar-item">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="sidebar-header">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => 'Single Event Sidebar',
        'id'            => 'sa-single-event',
        'description'   => __( 'Widgets in this area will be shown on all single events pages.', 'uastudentaffairs' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s sidebar-item">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="sidebar-header">',
        'after_title'   => '</h3>',
    ) );

});

// Print sidebars
add_action( 'sa_framework_print_left_sidebar', function() {

    // Get the sidebar ID
    $left_sidebar_id = apply_filters( 'sa_child_left_sidebar_id', apply_filters( 'sa_child_sidebar_id', 'sa-main' ) );

    if ( $left_sidebar_id && is_active_sidebar( $left_sidebar_id ) ) {
        dynamic_sidebar( $left_sidebar_id );
    }

}, 1 );

//! Setup styles and scripts
add_action( 'wp_enqueue_scripts', function () {
    global $sa_theme_dir, $sa_framework_dir;

    // Load Fonts
    // @TODO Check on fonts to make sure we're not loading unnecessary fonts
    //wp_enqueue_style( 'sa-child-fonts', 'https://fonts.googleapis.com/css?family=Bitter:400,400italic,700' );

    // Enqueue the theme stylesheet
    wp_enqueue_style( 'sa-child', $sa_theme_dir . 'css/sa-child.min.css', array( 'sa-fonts', 'dashicons' ) );

    // Enqueue the theme script
    wp_enqueue_script( 'sa-child', $sa_theme_dir . 'js/sa-child.min.js', array( 'jquery' ) );

    // Register handlebars
    wp_register_script( 'handlebars', '//cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js' );

    // Enqueue the home page styles
    if ( is_front_page() ) {

        // Enqueue the home styles
        wp_enqueue_style( 'sa-child-front', $sa_theme_dir . 'css/sa-child-front.min.css', array( 'sa-child' ) );

        // Enqueue the home script
        wp_enqueue_script( 'sa-child-front', $sa_theme_dir . 'js/sa-child-front.min.js', array( 'jquery', 'handlebars' ) );

    }

    // Register unveil and viewport
    wp_register_script( 'viewport', $sa_framework_dir . 'js/viewport.min.js', array( 'jquery' ) );
    wp_register_script( 'unveil', $sa_framework_dir . 'js/unveil.min.js', array( 'jquery', 'viewport' ) );

    // Enqueue the events styles
    if ( is_post_type_archive( 'tribe_events' ) || is_singular( 'tribe_events' ) ) {

		// Enqueue the base print styles
		wp_enqueue_style( 'sa-child-events', $sa_theme_dir . 'css/sa-child-events.min.css', array( 'sa-child', 'tribe-events-calendar-style' ), false );

	}

    // Enqueue the archive script
    if ( is_page( 'diversity-and-inclusion' ) ) {
        wp_enqueue_script( 'sa-child-archives', $sa_theme_dir . 'js/sa-child-archives.min.js', array( 'jquery', 'handlebars', 'sa-child' ), false, true );
    }

}, 20 );

// Add our own query vars
add_filter( 'query_vars', function( $vars ) {

    // For our people directory page
    $vars[] = 'people_dept';

    return $vars;
}, 10, 1 );

// Setup rewrites
add_action( 'init', function() {

    // Setup search landing page
    add_rewrite_rule( '^directory\/people\/(.+)/?', 'index.php?page_id=1227&people_dept=$matches[1]', 'top' );

});